import React from 'react';
import { Route, Routes } from "react-router";
import Header from "./components/layouts/Header";
import Footer from "./components/layouts/Footer"
import Homepage from "./pages/HomePage";
import Login from "./pages/Login";
import Transactions from "./pages/Transactions";
import Register from "./pages/Register";
import Dashboard from "./pages/Dashboard";

function App() {
    return (
      <React.Fragment>
        <div id="main-wrapper">
          <Header />
          <div id="content">
            <Routes>
              <Route path="/" element={<Homepage />}/>
              <Route path="/dashboard" element={<Dashboard />}/>
              <Route path="/transactions" element={<Transactions />}/>
              <Route path="/login" element={<Login />}/>
              <Route path="/register" element={<Register />}/>
              <Route path="/profile" element={<Login />}/>
            </Routes>
          </div>
          <Footer />
        </div>

      </React.Fragment>


    );
}

export default App;
