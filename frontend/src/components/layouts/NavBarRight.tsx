import React from "react";
import NavBarLink from "./NavBarLink";

interface NavBarRightProps {title?: string}

export default function NavBarRight({title}: NavBarRightProps){
  return (
    <nav className="login-signup navbar navbar-expand">
      <ul className="navbar-nav">
        <NavBarLink children={"Login"} to={"/login"} />
        <li className="align-items-center h-auto ml-sm-3">
          <NavBarLink className="btn btn-primary d-none d-sm-block" children={"Register"} to={"/register"} />
        </li>
      </ul>
    </nav>
  );
};
