import React from "react";
import Logo from "./Logo";
import NavbarToggleButton from "./NavBarToggleButton";
import Navbar from "./NavBar";
import NavBarRight from "./NavBarRight";

export type HeaderProps = { title: any }

export default function Header() {
  return (
    <header id="header">
      <div className="container">
        <div className="header-row">
          <div className="header-column justify-content-start">
            <Logo />
            <NavbarToggleButton />
            <Navbar />
          </div>
          <div className="header-column justify-content-end">
            <NavBarRight />
          </div>
        </div>
      </div>
    </header>
  );
}
