import React from "react";
import FooterTop from "./FooterTop";
import FooterBottom from "./FooterBottom";

export type FooterProps = { title?: string }

export default function Footer({ title }: FooterProps) {
  return (
    <footer id="footer">
      <div className="container">
        <FooterTop />
        <FooterBottom />
      </div>
    </footer>
  );
}
