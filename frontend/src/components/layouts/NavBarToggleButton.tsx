
export type NavbarToggleButtonProps = {title: string}

export default function NavbarToggleButton() {
    return (
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav">
          <span></span> <span></span> <span></span>
      </button>
    );
}
