import React, { useState } from "react";
import {  NavLink } from "react-router-dom";

export type NavBarLinkProps = { children: string; to: string; isActive?: Function, activeClassName?: string; className?: string | undefined; }

export default function NavBarLink(props: NavBarLinkProps) {
  useState()
  return (
      <>
        <li>
          <NavLink {...props} />
        </li>
       </>
    );
}
