import React from 'react';

export type FooterBottomProps = {title?: string}

export default function FooterBottom({title}: FooterBottomProps) {
    return (
      <div className="footer-copyright pt-3 pt-lg-2 mt-2">
          <div className="row">
              <div className="col-lg">
                  <p className="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2019 <a href="/">Payyed</a>. All
                      Rights Reserved.</p>
              </div>
              <div className="col-lg d-lg-flex align-items-center justify-content-lg-end">
                  <ul className="nav justify-content-center">
                      <li className="nav-item"><a className="nav-link active" href="/">Security</a></li>
                      <li className="nav-item"><a className="nav-link" href="/">Terms</a></li>
                      <li className="nav-item"><a className="nav-link" href="/">Privacy</a></li>
                  </ul>
              </div>
          </div>
      </div>
    );
}
