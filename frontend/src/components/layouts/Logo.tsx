import React from 'react';

export type LogoProps = {title: string}

export default function Logo() {
    return (
      <div className="logo"><a className="d-flex" href="/" title="Payyed - HTML Template">
          <img src="images/logo.png" alt="Payyed" /></a>
      </div>
    );
}
