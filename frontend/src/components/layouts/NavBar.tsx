import React from 'react';
import NavBarLink from "./NavBarLink";

export type NavbarProps = {
    title?: string
}

export default function Navbar() {
    return (
      <nav className="primary-menu navbar navbar-expand-lg">
        <div id="header-nav" className="collapse navbar-collapse">
            <div id="header-nav" className="collapse navbar-collapse">
                <ul className="navbar-nav mr-auto">
                    <NavBarLink children={"Home"} to={"/"} />
                    <NavBarLink children={"Pay Bills"} to={"/"}  />
                    <NavBarLink children={"Dashboard"} to={"/dashboard"}  />
                    <NavBarLink children={"Transactions"} to={"/transactions"}  />
                </ul>
            </div>
        </div>
      </nav>
    );
}
