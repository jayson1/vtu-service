import React from 'react';

export type WhyChooseUsProps = {}

export default function WhyChooseUsSection() {
    return (
      <section className="section bg-white">
          <div className="container">
              <h2 className="text-9 text-center text-uppercase font-weight-400">Why choose us?</h2>
              <p className="text-4 text-center font-weight-300 mb-5">Here’s Top 4 reasons why using a Payyed account for
                  manage your money.</p>
              <div className="row">
                  <div className="col-lg-10 mx-auto">
                      <div className="row">
                          <div className="col-sm-6 mb-4">
                              <div className="featured-box style-3">
                                  <div className="featured-box-icon border border-primary text-primary rounded-circle">
                                      <i className="fas fa-hand-pointer"></i></div>
                                  <h3 className="font-weight-400">Easy to use</h3>
                                  <p>Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent
                                      possim iriure.</p>
                              </div>
                          </div>
                          <div className="col-sm-6 mb-4">
                              <div className="featured-box style-3">
                                  <div className="featured-box-icon border border-primary text-primary rounded-circle">
                                      <i className="fas fa-share"></i></div>
                                  <h3 className="font-weight-400">Faster Payments</h3>
                                  <p>Persius interesset his et, in quot quidam persequeris vim, ad mea essent possim
                                      iriure.</p>
                              </div>
                          </div>
                          <div className="col-sm-6 mb-4 mb-sm-0">
                              <div className="featured-box style-3">
                                  <div className="featured-box-icon border border-primary text-primary rounded-circle">
                                      <i className="fas fa-dollar-sign"></i></div>
                                  <h3 className="font-weight-400">Lower Fees</h3>
                                  <p>Essent lisque persius interesset his et, in quot quidam persequeris vim, ad mea
                                      essent possim iriure.</p>
                              </div>
                          </div>
                          <div className="col-sm-6">
                              <div className="featured-box style-3">
                                  <div className="featured-box-icon border border-primary text-primary rounded-circle">
                                      <i className="fas fa-lock"></i></div>
                                  <h3 className="font-weight-400">100% secure</h3>
                                  <p>Quidam lisque persius interesset his et, in quot quidam persequeris vim, ad mea
                                      essent possim iriure.</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
    );
}
