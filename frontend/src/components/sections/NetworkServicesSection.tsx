import React from "react";

interface NetworkServicesProps {title?:string}
const NetworkServicesSection = ({title}: NetworkServicesProps) => {
  return (
    <section className="section">
      <div className="container overflow-hidden">
        <div className="row">
          <div className="col-lg-5 col-xl-6 d-flex">
            <div className="my-auto pb-5 pb-lg-0">
              <h2 className="text-9">Payment Solutions for anyone.</h2>
              <p className="text-4">Quidam lisque persius interesset his et, in quot quidam persequeris vim, ad mea
                essent possim iriure. Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent
                possim iriure. lisque persius interesset his et, in quot quidam mea essent possim iriure.</p>
              <a href="/" className="btn-link text-4">Find more solution<i
                className="fas fa-chevron-right text-2 ml-2"> </i></a></div>
          </div>
          <div className="col-lg-7 col-xl-6">
            <div className="row banner style-2 justify-content-center">
              <div className="col-12 col-sm-6 mb-4 text-center">
                <div className="item rounded shadow d-inline-block"><a href="/">
                  <div className="caption rounded-bottom">
                    <h2 className="text-5 font-weight-400 mb-0">Freelancer</h2>
                  </div>
                  <div className="banner-mask"> </div>
                  <img className="img-fluid" src="images/anyone-online-shopping.jpg" alt="banner" /> </a></div>
              </div>
              <div className="col-12 col-sm-6 mb-4 text-center">
                <div className="item rounded shadow d-inline-block"><a href="/">
                  <div className="caption rounded-bottom">
                    <h2 className="text-5 font-weight-400 mb-0">Online Shopping</h2>
                  </div>
                  <div className="banner-mask"></div>
                  <img className="img-fluid" src="images/anyone-online-shopping.jpg" alt="banner" /> </a></div>
              </div>
              <div className="col-12 col-sm-6 mb-4 mb-sm-0 text-center">
                <div className="item rounded shadow d-inline-block"><a href="/">
                  <div className="caption rounded-bottom">
                    <h2 className="text-5 font-weight-400 mb-0">Online Sellers</h2>
                  </div>
                  <div className="banner-mask"></div>
                  <img className="img-fluid" src="images/anyone-online-shopping.jpg" alt="banner" /> </a></div>
              </div>
              <div className="col-12 col-sm-6 text-center">
                <div className="item rounded shadow d-inline-block"><a href="/">
                  <div className="caption rounded-bottom">
                    <h2 className="text-5 font-weight-400 mb-0">Affiliate Marketing</h2>
                  </div>
                  <div className="banner-mask"> </div>
                  <img className="img-fluid" src="images/anyone-online-shopping.jpg" alt="banner" /> </a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NetworkServicesSection;
