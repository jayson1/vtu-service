import React from "react";

interface HomeQuickBuyProps {title?:string}
const HomeQuickBuySection = ({title}: HomeQuickBuyProps) => {
  return (
    <section className="hero-wrap section shadow-md pb-4">
      <div className="hero-mask opacity-7 bg-dark"><span /></div>
      <div className="hero-bg" style={{ backgroundImage: `url(images/image-5.jpg)`}}>
        <span /></div>
      <div className="hero-content py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-xl-7 my-auto text-center text-lg-left pb-4 pb-lg-0">
              <h2 className="text-14 font-weight-400 text-white mb-4">
                The Secure,<br className="d-none d-xl-block" />
                easiest and fastest <br className="d-none d-xl-block" />
                way to transfer money.
              </h2>
              <p className="text-4 text-white mb-4">Send & Receive money to your loved ones in minutes with great rates
                and low
                fees. Over 180 countries and 30 currencies supported.
              </p>
              <a href="/" className="btn-link text-4">See more details
                <i className="fas fa-chevron-right text-2 ml-2"><span/></i>
              </a>
            </div>
            <div className="col-lg-6 col-xl-5 my-auto">
              <ul className="nav nav-tabs nav-justified bg-white style-5 rounded-top" id="myTab" role="tablist">
                <li className="nav-item">
                  <a className="nav-link active" id="send-money-tab"
                     data-toggle="tab" href="#send-money" role="tab" aria-controls="send-money"
                     aria-selected="true">Send Money
                  </a>
                </li>
                <li className="nav-item"><a className="nav-link" id="request-money-tab" data-toggle="tab"
                                            href="#request-money" role="tab" aria-controls="request-money"
                                            aria-selected="false">Request Money</a></li>
              </ul>
              <div className="tab-content p-4 bg-white rounded-bottom" id="myTabContent">
                <div className="tab-pane fade show active" id="send-money" role="tabpanel"
                     aria-labelledby="send-money-tab">
                  <form id="form-send-money" method="post">
                    <div className="form-group">
                      <label htmlFor="youSend">You Send</label>
                      <div className="input-group">
                        <div className="input-group-prepend"><span className="input-group-text">$</span></div>
                        <input type="text" className="form-control" data-bv-field="youSend" id="youSend" value="1,000"
                               placeholder="" />
                          <div className="input-group-append"> <span className="input-group-text p-0">
                        <select id="youSendCurrency" data-style="custom-select bg-transparent border-0"
                                data-container="body" data-live-search="true"
                                className="selectpicker form-control bg-transparent" >
                          <optgroup label="Popular Currency">
                          <option data-icon="currency-flag currency-flag-usd mr-1" data-subtext="United States dollar"
                                  value="">USD</option>
                          <option data-icon="currency-flag currency-flag-aud mr-1" data-subtext="Australian dollar"
                                  value="">AUD</option>
                          <option data-icon="currency-flag currency-flag-inr mr-1" data-subtext="Indian rupee"
                                  value="">INR</option>
                          </optgroup>
                        </select>
                        </span></div>
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor="recipientGets">Recipient Gets</label>
                      <div className="input-group">
                        <div className="input-group-prepend"><span className="input-group-text">$</span></div>
                        <input type="text" className="form-control" data-bv-field="recipientGets" id="recipientGets"
                               value="1,410.06" placeholder="" />
                          <div className="input-group-append"> <span className="input-group-text p-0">
                        <select id="recipientCurrency" data-style="custom-select bg-transparent border-0"
                                data-container="body" data-live-search="true"
                                className="selectpicker form-control bg-transparent" >
                          <optgroup label="Popular Currency">
                          <option data-icon="currency-flag currency-flag-usd mr-1" data-subtext="United States dollar"
                                  value="">USD</option>
                          <option data-icon="currency-flag currency-flag-aud mr-1" data-subtext="Australian dollar"
                                   value="">AUD</option>
                          <option data-icon="currency-flag currency-flag-inr mr-1" data-subtext="Indian rupee"
                                  value="">INR</option>
                          </optgroup>

                        </select>
                        </span></div>
                      </div>
                    </div>
                    <p className="text-muted mb-1">Total fees - <span className="font-weight-500">7.21 USD</span></p>
                    <p className="text-muted">The current exchange rate is <span className="font-weight-500">1 USD = 1.42030 AUD</span>
                    </p>
                    <button className="btn btn-primary btn-block">Continue</button>
                  </form>
                </div>
                <div className="tab-pane fade" id="request-money" role="tabpanel" aria-labelledby="request-money-tab">
                  <form id="form-request-money" method="post">
                    <div className="form-group">
                      <label htmlFor="myCountrie">I am in</label>
                      <select id="myCountrie" data-style="custom-select" className="selectpicker form-control"
                              data-live-search="true">
                        <option value="244">Aaland Islands</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label htmlFor="mypayerCountrie">My payer is in</label>
                      <select id="mypayerCountrie" data-style="custom-select" className="selectpicker form-control"
                              data-live-search="true">
                        <option value="244">Aaland Islands</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label htmlFor="wantTorequest">I want to request</label>
                      <div className="input-group">
                        <div className="input-group-prepend"><span className="input-group-text">$</span></div>
                        <input type="text" className="form-control" data-bv-field="wantTorequest" id="wantTorequest"
                               value="1,000" placeholder="" />
                          <div className="input-group-append"> <span className="input-group-text p-0">
                        <select id="wantToCurrency" data-style="custom-select bg-transparent border-0"
                                data-container="body" data-live-search="true"
                                className="selectpicker form-control bg-transparent" >
                          <optgroup label="Popular Currency">
                          <option data-icon="currency-flag currency-flag-usd mr-1" data-subtext="United States dollar"
                                   value="">USD</option>
                          <option data-icon="currency-flag currency-flag-aud mr-1" data-subtext="Australian dollar"
                                  value="">AUD</option>
                          <option data-icon="currency-flag currency-flag-inr mr-1" data-subtext="Indian rupee"
                                  value="">INR</option>
                          </optgroup>
                        </select>
                        </span></div>
                      </div>
                    </div>
                    <button className="btn btn-primary btn-block mt-4">Continue</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HomeQuickBuySection;
