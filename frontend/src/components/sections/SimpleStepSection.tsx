import React from "react";

interface SimpleStepProps {title?:string}
const SimpleStep = ({title}: SimpleStepProps) => {
  return (
    <section className="section">
      <div className="container">
        <h2 className="text-9 text-center text-uppercase font-weight-400">As simple as 1-2-3</h2>
        <p className="text-4 text-center font-weight-300 mb-5">Lorem Ipsum is simply dummy text of the printing and
          typesetting industry.</p>
        <div className="row">
          <div className="col-sm-4 mb-4">
            <div className="featured-box style-4">
              <div className="featured-box-icon text-dark shadow-none border-bottom"><span
                className="w-100 text-20 font-weight-500">1</span></div>
              <h3 className="mb-3">Sign Up Your Account</h3>
              <p className="text-3 font-weight-300">Sign up for your free personal/business Account in fea a minute.</p>
            </div>
          </div>
          <div className="col-sm-4 mb-4">
            <div className="featured-box style-4">
              <div className="featured-box-icon text-dark shadow-none border-bottom"><span
                className="w-100 text-20 font-weight-500">2</span></div>
              <h3 className="mb-3">Send & Receive Money</h3>
              <p className="text-3 font-weight-300">Receive & Send Payments from worldwide in 40 currencies.</p>
            </div>
          </div>
          <div className="col-sm-4 mb-4 mb-sm-0">
            <div className="featured-box style-4">
              <div className="featured-box-icon text-dark shadow-none border-bottom"><span
                className="w-100 text-20 font-weight-500">3</span></div>
              <h3 className="mb-3">Withdraw Funds</h3>
              <p className="text-3 font-weight-300">Your funds will be transferred to your local bank account.</p>
            </div>
          </div>
        </div>
        <div className="text-center mt-2"><a href="/" className="btn btn-primary">Open a Free Account</a></div>
      </div>
    </section>
  );
};

export default SimpleStep;
