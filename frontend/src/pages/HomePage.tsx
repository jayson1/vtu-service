import React from 'react';
import HomeQuickBuySection from "../components/sections/HomeQuickBuySection";
import NetworkServicesSection from "../components/sections/NetworkServicesSection";
import WhyChooseUsSection from "../components/sections/WhyChooseUsSection";
import SimpleStepSection from "../components/sections/SimpleStepSection";
import WhatCanYouDo from "../components/sections/WhatCanYouDo";

export type HomepageProps = {}

export default function Homepage() {
    return (
      <React.Fragment>
          <HomeQuickBuySection />
          <NetworkServicesSection />
          <WhyChooseUsSection />
          <SimpleStepSection />
          <WhatCanYouDo />
      </React.Fragment>

    );
}
