import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "./vendor/bootstrap/css/bootstrap.min.css"
import "./vendor/owl.carousel/assets/owl.carousel.min.css"
import "./vendor/stylesheet.css"
import "./vendor/font-awesome/css/all.min.css"
import "./index.css";


const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
