import {
    EmailTemplateEnum,
    ServiceCategoryEnum,
    ServicePartnerEnum,
    ServicePartnerAuthTypeEnum,
    UserTypeEnum,
    SettingSectionEnum,
    PlanHierarchyEnum,
    StatusEnum,
    SettingValueTypeEnum,
    ProviderItemEnum,
    BillerIdentifierEnum,
} from './enums.payload';
import { ImagePayload } from './classes.payload';
import { HttpStatus } from '@nestjs/common';
import { ItemService } from '../../item/entity/item-service.entity';
import { Column } from 'typeorm';

export interface IResponse {
    statusCode: HttpStatus;
    message: string;
}

export interface IJwtToken {
    email: string;
    token: string;
    type: UserTypeEnum;
    uuid: string;
}

export interface IJwtAuth {
    email: string;
    uuid: string;
}

export interface IUserPreference {
    allowProfileView: boolean;
}

export interface iItemService {
    id?: number;
    uuid?: string;
    title?: string;
    slug?: string;
    order?: number;
    variationLabel: string;
    description?: string;
    status: StatusEnum;
    minAmount?: number;
    maxAmount?: number;
    canEditVariantAmount: boolean;
    isValidateBillerCode: boolean;
    provider: ProviderItemEnum; // service provider (ie if service is data, provider will be MTN, GLO, Airtel)
    itemCategory: ServiceCategoryEnum; // category of this service (ie, DATA, AIRTIME, ELECTRICITY etc)
    convenienceFee?: any;
    billerIdentifier: BillerIdentifierEnum;
    conversionStatus?: string;
    imageUrl?: string;
    shareNumber?: string;
    shareInfo?: string;
    shareRate?: string;
    moreInfo?: string;
    serviceVariants: iItemServiceVariant[];
}

export interface iItemServiceVariant {
    id?: number;
    uuid?: string;
    amount: number;
    isCommissionCheck: boolean;
    display?: string;
    isFixedPrice: boolean;
    title: string;
    order: number;
    itemService?: ItemService;
    slug: string;
    status: StatusEnum;
}

export interface iItemServiceCategory {
    title: string;
    identifier: ServiceCategoryEnum;
    statusEnum: StatusEnum;
    id?: number;
    uuid?: string;
    code?: string;
    itemService?: iItemService[];
    planRank?: number;
}

export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    type: UserTypeEnum;
    password: string;
    address?: string;
    allowNotification?: boolean;
    designation?: string;
    salt?: string;
    dob?: string;
    preference?: IUserPreference;
    image?: ImagePayload;
    city?: string;
    state?: string;
    country?: string;
    id?: number;
    uuid?: string;
    isVerified?: boolean;
    isActive?: boolean;
    isAdmin?: boolean;
    walletId?: string;
    username: string;
}

export interface iItemServicePartner {
    id?: number;
    uuid?: string;
    code?: string;
    title: string;
    description: string;
    authUsername?;
    authPassword?;
    authToken?;
    websiteUrl?: string;
    liveBaseUrl?: string;
    sandboxBaseUrl?: string;
    authType?: ServicePartnerAuthTypeEnum;
    others?: any;
    serviceEndpoint?: IServicePartnerEndpoint;
    enumIdentifier: ServicePartnerEnum;
    publicKey?: string;
    secretKey?: string;
}

export interface IServicePartnerEndpoint {
    data?: string;
    cable?: string;
    waecRegistration?: string;
    waecResultChecker?: string;
    airtime?: string;
    electricity?: string;
    transfer?: string;
    airToCash?: string;
    necoResultCheckerPin?: string;
    generateRechargeCardPin?: string;
    variationCode?: string;
}

export interface IEncrypt {
    iv: string;
    content: string;
}

export interface IEmailProps<T> {
    receiverName: string;
    receiverEmail: string;
    emailSubject: string;
    emailTemplate: EmailTemplateEnum;
    data: T;
}

export interface IConfirmEmailData {
    userName: string;
    userEmail: string;
    userLink: string;
    siteProps: ISiteProps;
}

export interface ISiteProps {
    siteLogoUrl: string;
    siteBaseUrl: string;
    siteSupportEmail: string;
    siteAddress: string;
    siteContactUrl: string;
    siteName: string;
}

export interface IPlan {
    id?: number;
    uuid?: string;
    title: string;
    pricePerDay: number;
    pricePerMonth: number;
    pricePerYear: number;
    hierarchy: number;
    settings?: ISetting[];
}

export interface ISetting {
    uuid?: string;
    key: string;
    value: string;
    section: SettingSectionEnum;
    planRank?: PlanHierarchyEnum;
    type: SettingValueTypeEnum;
}

export interface iItemProvider {
    id?: number;
    uuid?: string;
    title: string;
    status: StatusEnum;
    itemCategory?: iItemServiceCategory;
    itemService?: iItemService[];
    serviceCategoryEnum: ServiceCategoryEnum;
}

export interface IPartnerCategory {
    id?: number;
    uuid?: string;
    itemCategoryEnum: ServiceCategoryEnum;
    settingPartnerEnum: ServicePartnerEnum;
    statusEnum: StatusEnum;
    partnerServiceId: string;
    providerEnum: ProviderItemEnum;
}

export interface IPartnerService {
    id?: number;
    uuid?: string;
    itemCategory: ServiceCategoryEnum;
    settingPartner: ServicePartnerEnum;
    itemServiceSlug: string;
    itemServiceVariantSlug: string;
    status: StatusEnum.ACTIVE;
    partnerServiceId: string;
    costPrice: number;
    sellingPrice: number;
    profitMarginPercent: number;
    profitAmount: number;
    agentPrice: number;
    regularPrice: number;
}
