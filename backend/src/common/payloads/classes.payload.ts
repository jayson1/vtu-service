import { Equals, IsEmail, IsNotEmpty, IsNumberString, IsString, IsUrl } from 'class-validator';
import { Match } from '../util/decorators/match.decorator';
import { SettingKeyEnum } from './enums.payload';
import { User } from '../../user/entity/user.entity';

export class ImagePayload {
    //todo replace usage with VtuMediaFile
    @IsNotEmpty()
    public_id: string;

    @IsNotEmpty()
    @IsUrl()
    secure_url: string;
}

export class SignUpPayload {
    @IsString()
    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    phone: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}

export class SignInPayload {
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}

export class EmailVerifyPayload {
    @IsNotEmpty()
    token: string;

    @IsNotEmpty()
    uuid: string;

    @IsNotEmpty()
    email: string;
}

export class SettingPayload {
    @IsNotEmpty()
    key: SettingKeyEnum;
    value: string;
}

export class UserPayload {
    uuid: string;
    email: string;
    phone: string;
    firstName: string;
    lastName: string;
    password: string;

    constructor(email?: string, phone?: string, password?: string) {
        this.email = email;
        this.phone = phone;
        this.password = password;
    }
}

export class PasswordResetPayload {
    @IsNotEmpty()
    oldPassword: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    @Match('password')
    confirmPassword: string;
}

export class WalletPayload {
    user: User;
    balance: number;

    constructor(user?: User, balance?: number) {
        this.user = user;
        this.balance = balance;
    }
}
