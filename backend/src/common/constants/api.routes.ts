export class ApiRoutes {
    public static readonly API_PREFIX = 'api'; // set globally in main.ts
    public static readonly API_VERSION = 'v1';
    public static readonly ENDPOINT = `${ApiRoutes.API_VERSION}`;

    /* AUTH ENDPOINT */
    public static readonly AUTH_ROUTE = `${ApiRoutes.ENDPOINT}/auth/`;
    public static readonly AUTH_SIGN_UP = `sign-up`;
    public static readonly AUTH_SIGN_IN = `sign-in`;
    public static readonly AUTH_EMAIL_VERIFY = 'account/verification';
    public static readonly AUTH_UPDATE_PASSWORD = 'account/password/update/:uui';
    public static readonly AUTH_SEND_RESET_PASSWORD_EMAIL = 'account/password/reset/:email';
    public static readonly AUTH_RESET_PASSWORD = 'account/password/reset';
    public static readonly AUTH_RESET_LINK_PREFIX = 'account/password/reset';
    public static readonly AUTH_RESET_TOKEN_VERIFY = 'account/password/reset/:token/:email';

    /* USERS ENDPOINT */
    public static readonly USERS_ROUTE = `${ApiRoutes.ENDPOINT}/users/`;
    public static readonly USERS_GET_ALL = `all`;
    public static readonly USERS_GET_BY_UUID = `:uui`;

    /* SERVICE CATEGORY */
    public static readonly SERVICE_CATEGORY_ROUTE = `${ApiRoutes.ENDPOINT}/service-category`;
    public static readonly SERVICE_CATEGORY_GET_ALL = `/get-all`;

    /* SERVICE ENDPOINT */
    public static readonly SERVICE_ROUTE = `${ApiRoutes.ENDPOINT}/services`;

    /* SERVICE PROVIDER ENDPOINT */
    public static readonly SERVICE_PROVIDER_ROUTE = `${ApiRoutes.ENDPOINT}/service-provider`;
    public static readonly SERVICE_PROVIDER_GET_ALL = `/all`;

    /* SETTING ENDPOINT */
    public static readonly SETTING_ROUTE = `${ApiRoutes.ENDPOINT}/settings`;
    public static readonly SETTING_GET_ALL = `/all`;
    public static readonly SETTING_SETUP = `/setup`;
    public static readonly SETTING_UPDATE = `/update`;

    /* SEED ENDPOINT */
    public static readonly SEED_ROUTE = `${ApiRoutes.ENDPOINT}/seed`;
    public static readonly SEED_USER = `/user`;
    public static readonly SEED_SERVICES = `/services`;
    public static readonly SEED_SERVICE_CATEGORIES = `/service-categories`;
    public static readonly SEED_SERVICE_PROVIDERS = `/service-providers`;
    public static readonly SEED_GLOBAL_SETTINGS = `/global-settings`;

    /* ITEM SERVICE ENDPOINT */
    public static readonly ITEM_ROUTE = `${ApiRoutes.ENDPOINT}/item`;

    /* PARTNER ENDPOINT */
    public static readonly PARTNER_ROUTE = `${ApiRoutes.ENDPOINT}/partner`;
}
