export class TableNames {
    public static readonly TBL_PR = 'vtu_';
    public static readonly TBL_SUF = '_data';

    // item module tables properties
    public static readonly ITEM_CATEGORY_TBL = `${this.TBL_PR}item_categories${this.TBL_SUF}`;
    public static readonly ITEM_PROVIDER_TBL = `${this.TBL_PR}item_providers${this.TBL_SUF}`;
    public static readonly ITEM_SERVICE_TBL = `${this.TBL_PR}item_services${this.TBL_SUF}`;
    public static readonly ITEM_VARIANT_TBL = `${this.TBL_PR}item_variants${this.TBL_SUF}`;

    // settings module tables props
    public static readonly SETTING_TBL = `${this.TBL_PR}settings${this.TBL_SUF}`;

    // partner module tables properties
    public static readonly PARTNER_TBL = `${this.TBL_PR}partner${this.TBL_SUF}`;
    public static readonly PARTNER_CATEGORY_TBL = `${this.TBL_PR}partner_categories${this.TBL_SUF}`;
    public static readonly PARTNER_SERVICE_TBL = `${this.TBL_PR}partner_services${this.TBL_SUF}`;

    // users module table props
    public static readonly USERS_TBL = `${this.TBL_PR}users${this.TBL_SUF}`;

    // wallet module table props
    public static readonly WALLET_TBL = `${this.TBL_PR}wallets${this.TBL_SUF}`;
    public static readonly WALLET_HISTORY_TBL = `${this.TBL_PR}wallet_histories${this.TBL_SUF}`;

    // auth module table props
    public static readonly PASSWORD_RESET_TBL = `${this.TBL_PR}auth_password_resets${this.TBL_SUF}`;
    public static readonly VERIFICATION_TBL = `${this.TBL_PR}auth_verifications${this.TBL_SUF}`;

    // transactions module
    public static readonly TRANSACTION_TBL = `${this.TBL_PR}transactions${this.TBL_SUF}`;

    // subscription module
    public static readonly PLAN_TBL = `${this.TBL_PR}subscription_plans${this.TBL_SUF}`;
    public static readonly SUBSCRIPTION_TBL = `${this.TBL_PR}subscriptions${this.TBL_SUF}`;
    public static readonly SUBSCRIPTION_HISTORY_TBL = `${this.TBL_PR}subscription_histories${this.TBL_SUF}`;
}
