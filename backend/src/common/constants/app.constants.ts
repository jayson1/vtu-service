export class ValidationConstant {
	public static readonly DUP_CODE = '23505';
	public static readonly DUP_TEXT = 'ER_DUP_ENTRY';
	public static readonly DUPLICATE = 'POSSIBLY DUPLICATES';
	public static readonly EMAIL_EXIST = 'Email Already Exist!';
	public static readonly USER_NOT_FOUND = 'USER NOT FOUND!';
	public static readonly INVALID_CREDENTIALS = 'INVALID CREDENTIALS';
	public static readonly INVALID_EMAIL = 'INVALID EMAIL';
	public static readonly ACCOUNT_NOT_VERIFIED =
		'VERIFY YOUR ACCOUNT TO CONTINUE, CHECK YOUR EMAIL.';
	public static readonly ACCOUNT_DISABLED = 'Your account has been temporarily disabled';

	public static readonly EMAIL_CONFIRM_SUBJECT = 'ACCOUNT VERIFICATION';
	public static readonly VERIFICATION_SENT = 'VERIFICATION EMAIL SENT SUCCESSFULLY';
	public static readonly VERIFICATION_SUCCESS = 'ACCOUNT VERIFICATION SUCCESSFUL';
	public static readonly SIGNUP_SUCCESS = 'Account Registration Successful';

	public static readonly ACTION_SUCCESSFUL = 'ACTION SUCCESSFUL';
	public static readonly ACTION_FAILED = 'ACTION FAILED';
	public static readonly NO_PENDING_VERIFICATION = 'No Pending Account Verification.';

	public static readonly UPDATED_SUCCESSFUL = 'UPDATED SUCCESSFULLY';
	public static readonly CANNOT_UPDATE_EMPTY = 'CANNOT UPDATE EMPTY OBJECT';

	public static readonly UNABLE_TO_SAVE = 'UNABLE TO SAVE';
	public static readonly SAVED_SUCCESSFUL = 'SAVED SUCCESSFULLY';

	public static readonly NOT_FOUND_EXCEPTION = 'NO RECORD FOUND! ';
	public static readonly EMAIL_VERIFICATION_FAILED = `Email Verification Failed to Process`;
	public static readonly RESET_LINK_EXPIRED = 'LINK EXPIRED, TRY AGAIN';
	public static readonly RESET_LINK_VALID = 'VALID';
	public static readonly RESET_LINK_INVALID = 'INVALID LINK';
	public static readonly RESET_TOKEN_INVALID = 'INVALID RESET TOKEN';
	public static readonly RESET_VERIFICATION_EMAIL_SENT = 'Reset Password Verification Link Sent';

	//
	public static readonly CANNOT_BE_EMPTY = 'Payload Cannot Be Empty';
	public static readonly SERVICES_EMPTY = 'Service Categories Cannot Be Empty';
	/* RESPONSE MESSAGES */
	public static readonly API_SERVER_RESPONSE_TIMEOUT =
		'Transaction Processed, Do Not Retry. Kindly Contact The Administrator';
	public static readonly API_SERVER_EMPTY_RESPONSE =
		'Processed Transactions, Do Not Retry. Contact Admin';
	public static readonly API_FAILED_TRANSACTION =
		'Transaction-Not Resolved, Try again or Contact Admin!!!';
	public static readonly API_WEBSITE_SCRIPT_TIME_OUT =
		'Time Out, Kindly Contact The Administrator';
	public static readonly EMAIL_VERIFICATION_SENT = `Email verification is required, kindly check your email to verify your account`;
	public static readonly OLD_PASSWORD_MISMATCH = `Old Password Mismatch`;
}
