import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import * as crypto from 'crypto';
import { ConfigService } from '@nestjs/config';
import { v4 as uuidv4 } from 'uuid';
import * as bcrypt from 'bcrypt';
import { MailJetService } from '../util/mail-jet/mail-jet.service';
import { MailSmtpService } from '../util/mail-smtp/mail-smtp.service';
import { User } from '../../user/entity/user.entity';
import { IConfirmEmailData, IResponse, ISiteProps } from '../payloads/interface.payload';
import { ValidationConstant } from '../constants/app.constants';
import { SettingService } from '../../setting/service/setting.service';
import { SettingKeyEnum, SettingSectionEnum } from '../payloads/enums.payload';
import { Setting } from '../../setting/entity/setting.entity';
import { ApiRoutes } from '../constants/api.routes';
import { PasswordReset } from '../../auth/entity/password-reset.entity';

@Injectable()
export class CommonService {
    private LOGGER = new Logger(CommonService.name);

    private algorithm: string;
    private secretKey: string;
    private iv = crypto.randomBytes(16);

    constructor(
        private configService: ConfigService,
        private mailJetService: MailJetService,
        private mailSmtpService: MailSmtpService,
        private settingService: SettingService
    ) {
        this.algorithm = this.getConfigValue('ENCRYPT_ALGORITHM');
        this.secretKey = this.getConfigValue('ENCRYPT_SECRET_KEY');
    }

    public encrypt = (text) => {
        const cipher = crypto.createCipheriv(this.algorithm, this.secretKey, this.iv);
        const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
        return {
            iv: this.iv.toString('hex'),
            content: encrypted.toString('hex'),
        };
    };

    public decrypt = (hash) => {
        const decipher = crypto.createDecipheriv(
            this.algorithm,
            this.secretKey,
            Buffer.from(hash.iv, 'hex')
        );
        const decrypted = Buffer.concat([
            decipher.update(Buffer.from(hash.content, 'hex')),
            decipher.final(),
        ]);
        return decrypted.toString();
    };

    public async generateRequestToken(): Promise<string> {
        const cs = new Date().toLocaleString('en-US', { timeZone: 'Africa/Lagos' });

        const today = new Date(cs);
        const year = today.getFullYear();

        const mon = today.getMonth() + 1;
        const month = mon < 10 ? `0${mon}` : `${mon}`;

        const da = today.getDate();
        const day = da < 10 ? `0${da}` : `${da}`;

        const hours = today.getHours();
        const min = today.getMinutes();
        return `${year}${month}${day}${hours}${min}${Math.floor(Math.random() * 109500)}`;
    }

    public async generateToken(): Promise<string> {
        return await uuidv4();
    }

    public async generateSalt(): Promise<string> {
        return await bcrypt.genSalt();
    }

    public async hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
    }

    public getConfigValue(key: string): string {
        return this.configService.get<string>(key);
    }

    /**
     * Send account verification email to user
     * @param user
     * @param token
     * @return void
     */
    public async sendAccountVerificationEmail(user: User, token: string): Promise<IResponse> {
        try {
            const payload: IConfirmEmailData = {
                userEmail: user.email,
                userName: user.username,
                userLink: await this.getVerificationLink(token, user.uuid),
                siteProps: await this.getSiteProps(),
            };
            await this.mailSmtpService.sendUserConfirmation(payload);
            return { message: ValidationConstant.VERIFICATION_SENT, statusCode: HttpStatus.OK };
        } catch (e) {
            this.LOGGER.error(e);
            return {
                message: ValidationConstant.EMAIL_VERIFICATION_FAILED,
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
            };
        }
    }

    public async sendResetPasswordEmail(
        email: string,
        username: string,
        savedReset: PasswordReset
    ): Promise<IResponse> {
        const props = await this.getSiteProps();
        await this.mailSmtpService.sendResetPasswordVerification(
            props,
            username,
            savedReset.link,
            email
        );
        return {
            message: ValidationConstant.RESET_VERIFICATION_EMAIL_SENT,
            statusCode: HttpStatus.OK,
        };
    }

    public async getVerificationLink(token: string, uuid: string): Promise<string> {
        const base: string = await this.getSettingValue(SettingKeyEnum.WEBSITE_BASE_URL);
        return `${base}/${token}/${uuid}`;
    }

    public async getSettingValue(key: SettingKeyEnum): Promise<string> {
        const setting = await this.settingService.findByKey(key);
        return setting.value;
    }

    public async getSettingValuesBySection(section: SettingSectionEnum): Promise<Setting[]> {
        return await this.settingService.findAllBySection(section);
    }

    public async generateResetPasswordLink(email: string, token: string): Promise<string> {
        const siteBaseUrl = await this.getSettingValue(SettingKeyEnum.WEBSITE_BASE_URL);
        return `${siteBaseUrl}/${ApiRoutes.AUTH_RESET_LINK_PREFIX}/${email}/${token}`;
    }

    public generateExpirationDate(): Date {
        const date = new Date();
        return new Date(date.getTime() + 1000 * 60 * 60); // expire after one hour
    }

    public async getSiteProps() {
        //todo:: replace with single fetch using setting section (getAllBySection(WEBSITE);
        const props: ISiteProps = {
            siteAddress: await this.getSettingValue(SettingKeyEnum.WEBSITE_COMPANY_ADDRESS),
            siteBaseUrl: await this.getSettingValue(SettingKeyEnum.WEBSITE_BASE_URL),
            siteContactUrl: await this.getSettingValue(SettingKeyEnum.WEBSITE_CONTACT_US_URL),
            siteLogoUrl: await this.getSettingValue(SettingKeyEnum.WEBSITE_COMPANY_LOGO_URL),
            siteSupportEmail: await this.getSettingValue(
                SettingKeyEnum.WEBSITE_SUPPORT_EMAIL_ADDRESS
            ),
            siteName: await this.getSettingValue(SettingKeyEnum.WEBSITE_COMPANY_NAME),
        };
        return props;
    }
}
