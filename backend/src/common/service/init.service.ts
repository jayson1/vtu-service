import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { CommonService } from './common.service';
import { PlanService } from '../../subscription/service/plan.service';
import {
    seedItemProviders,
    seedPartnerServices,
    seedPlan,
    seedServiceCategory,
    seedPartnerCategory,
    seedServiceProvider,
    seedServices,
    seedSetting,
    seedSystemUser,
} from '../payloads/seed-data';
import { Plan } from '../../subscription/entity/plan.entity';
import { ItemService } from '../../item/entity/item-service.entity';
import { Setting } from '../../setting/entity/setting.entity';
import { SettingService } from '../../setting/service/setting.service';
import { Partner } from '../../partner/entity/partner.entity';
import { PartnerService } from '../../partner/service/partner.service';
import { ItemCategory } from '../../item/entity/item-category.entity';
import { ItemCategoryService } from '../../item/service/item-category.service';
import { ItemProviderService } from '../../item/service/item-provider.service';
import { ItemProvider } from '../../item/entity/item-provider.entity';
import { ItemServiceImpl } from '../../item/service/item-service-impl.service';
import { iItemServiceVariant } from '../payloads/interface.payload';
import { ItemServiceVariant } from '../../item/entity/item-service-variant.entity';
import { PartnerItemCategoryService } from '../../partner/service/partner-item-category.service';
import { PartnerItemServiceImplService } from '../../partner/service/partner-item-service-impl.service';
import { UserService } from '../../user/service/user.service';
import { PartnerItemCategory } from '../../partner/entity/partner-item-category.entity';
import { PartnerItemService } from '../../partner/entity/partner-item-service.entity';
import { StatusEnum, UserTypeEnum } from '../payloads/enums.payload';
import { User } from '../../user/entity/user.entity';

@Injectable({})
export class InitService implements OnApplicationBootstrap {
    private logger = new Logger(InitService.name);

    constructor(
        private planService: PlanService,
        private commonService: CommonService,
        private settingService: SettingService,
        private partnerService: PartnerService,
        private itemCategoryService: ItemCategoryService,
        private itemProviderService: ItemProviderService,
        private itemService: ItemServiceImpl,
        private itemPartnerCategoryService: PartnerItemCategoryService,
        private itemPartnerServiceImpl: PartnerItemServiceImplService,
        private userService: UserService
    ) {}

    public async onApplicationBootstrap(): Promise<void> {
        await this.seedPlans();
        await this.seedSettings();
        await this.seedPartners();
        await this.seedServiceItemCategories();
        await this.seedServiceProviders();
        await this.seedServices();
        await this.seedPartnerItemCategory();
        await this.seedPartnerItemService();
        await this.seedSuperAdminUser();
    }

    private async seedPlans(): Promise<void> {
        if (await this.planService.canRunSeeder()) {
            try {
                const plans: Plan[] = [];
                for (const plan of seedPlan) {
                    const { title, pricePerMonth, pricePerDay, pricePerYear, hierarchy } = plan;
                    const newPlan: Plan = new Plan();
                    newPlan.title = title;
                    newPlan.hierarchy = hierarchy;
                    newPlan.pricePerDay = pricePerDay;
                    newPlan.pricePerMonth = pricePerMonth;
                    newPlan.uuid = await this.commonService.generateToken();
                    newPlan.pricePerYear = pricePerYear;
                    plans.push(newPlan);
                }
                const seeded = await this.planService.seedPlans(plans);
                if (seeded) this.logger.debug('-------- PLAN SEEDED SUCCESSFULLY -----------');
            } catch (e) {
                this.logger.error('-------- SEEDING PLAN FAILED -----------');
            }
        }
    }

    private async seedSettings(): Promise<void> {
        if (await this.settingService.canRunSeeder()) {
            try {
                const settings: Setting[] = [];
                for (const setting of seedSetting) {
                    const newSetting: Setting = new Setting();
                    newSetting.uuid = await this.commonService.generateToken();
                    newSetting.key = setting.key;
                    newSetting.value = setting.value;
                    newSetting.section = setting.section;
                    newSetting.plan = await this.planService.getPlanByRank(setting.planRank);
                    newSetting.type = setting.type;
                    settings.push(newSetting);
                }
                const seeded = await this.settingService.seedSettings(settings);
                if (seeded) this.logger.debug('-------- SETTINGS SEEDED SUCCESSFULLY -----------');
            } catch (e) {
                this.logger.error('-------- SEEDING SETTINGS FAILED -----------');
            }
        }
    }

    private async seedPartners(): Promise<void> {
        if (await this.partnerService.canRunSeeder()) {
            try {
                const partners: Partner[] = [];
                for (const partner of seedServiceProvider) {
                    const newPartner: Partner = new Partner();
                    newPartner.uuid = await this.commonService.generateToken();
                    newPartner.title = partner.title;
                    newPartner.description = partner.description;
                    newPartner.websiteUrl = partner.websiteUrl;
                    newPartner.liveBaseUrl = partner.liveBaseUrl;
                    newPartner.serviceEndpoint = partner.serviceEndpoint;
                    if (partner.authUsername) {
                        newPartner.authUsername = this.commonService.encrypt(
                            partner.authUsername
                        ).content;
                    }
                    if (partner.authPassword) {
                        newPartner.authPassword = this.commonService.encrypt(
                            partner.authPassword
                        ).content;
                    }
                    if (partner.authToken) {
                        newPartner.authToken = this.commonService.encrypt(
                            partner.authToken
                        ).content;
                    }
                    if (partner.publicKey) {
                        newPartner.publicKey = this.commonService.encrypt(
                            partner.publicKey
                        ).content;
                    }
                    if (partner.secretKey) {
                        newPartner.secretKey = this.commonService.encrypt(
                            partner.secretKey
                        ).content;
                    }

                    newPartner.authType = partner.authType;
                    newPartner.identifier = partner.enumIdentifier;
                    partners.push(newPartner);
                }
                const seeded = await this.partnerService.seedSettingPartner(partners);
                if (seeded) {
                    this.logger.debug('-------- SETTING PARTNERS SEEDED SUCCESSFULLY -----------');
                }
            } catch (e) {
                this.logger.error('-------- SEEDING SETTING PARTNERS FAILED -----------');
            }
        }
    }

    private async seedServiceItemCategories(): Promise<void> {
        if (await this.itemCategoryService.canRunSeeder()) {
            try {
                const categories: ItemCategory[] = [];
                for (const category of seedServiceCategory) {
                    const seed: ItemCategory = new ItemCategory();
                    seed.uuid = await this.commonService.generateToken();
                    seed.title = category.title;
                    seed.identifier = category.identifier;
                    seed.code = category.code;
                    seed.status = category.statusEnum;
                    categories.push(seed);
                }
                const isSeeded = await this.itemCategoryService.seedItemCategories(categories);
                if (isSeeded)
                    this.logger.debug('-------- ITEM CATEGORIES SEEDED SUCCESSFULLY -----------');
            } catch (e) {
                this.logger.debug('-------- ITEM CATEGORIES SEED FAILED -----------');
            }
        }
    }

    private async seedServiceProviders(): Promise<void> {
        if (await this.itemProviderService.hasNoData()) {
            try {
                const providers: ItemProvider[] = [];
                for (const provider of seedItemProviders) {
                    const newProvider: ItemProvider = new ItemProvider();
                    newProvider.uuid = await this.commonService.generateToken();
                    newProvider.title = provider.title;
                    newProvider.status = provider.status;
                    newProvider.itemCategory = await this.itemCategoryService.getByIdentifier(
                        provider.serviceCategoryEnum
                    );
                    providers.push(newProvider);
                }
                const seeded = await this.itemProviderService.seedProviders(providers);
                if (seeded) {
                    this.logger.debug('-------- ITEM PROVIDER SEED SUCCESSFULLY -----------');
                }
            } catch (e) {
                this.logger.debug('-------- ITEM PROVIDER SEED FAILED -----------');
            }
        }
    }

    private async seedServices(): Promise<void> {
        if (await this.itemService.canRunSeeder()) {
            try {
                for (const service of seedServices) {
                    const newService: ItemService = new ItemService();
                    newService.uuid = await this.commonService.generateToken();
                    newService.title = service.title;
                    newService.slug = service.slug;
                    newService.variationLabel = service.variationLabel;
                    newService.status = service.status;
                    newService.minAmount = service.minAmount;
                    newService.maxAmount = service.maxAmount;
                    newService.canEditVariantAmount = service.canEditVariantAmount;
                    newService.isValidateBillerCode = service.isValidateBillerCode;
                    newService.provider = await this.itemProviderService.getProviderByTitle(
                        service.provider
                    );
                    newService.itemCategory = await this.itemCategoryService.getByIdentifier(
                        service.itemCategory
                    );
                    newService.convenienceFee = service.convenienceFee;
                    newService.billerIdentifier = service.billerIdentifier;
                    newService.imageUrl = service.imageUrl;
                    const itemService = await this.itemService.createService(newService);
                    await this.seedServiceVariants(itemService, service.serviceVariants);
                }
                this.logger.debug('-------- ITEM SERVICE SEED SUCCESSFULLY -----------');
            } catch (e) {
                this.logger.debug('-------- ITEM SERVICE SEED FAILED -----------');
            }
        }
    }

    private async seedServiceVariants(
        service: ItemService,
        serviceVariants: iItemServiceVariant[]
    ): Promise<void> {
        try {
            const variants: ItemServiceVariant[] = [];
            for (const variant of serviceVariants) {
                const newServiceVariant = new ItemServiceVariant();
                newServiceVariant.uuid = await this.commonService.generateToken();
                newServiceVariant.amount = variant.amount;
                newServiceVariant.isCommissionCheck = variant.isCommissionCheck;
                newServiceVariant.isFixedPrice = variant.isFixedPrice;
                newServiceVariant.title = variant.title;
                newServiceVariant.itemService = service;
                newServiceVariant.slug = variant.slug;
                newServiceVariant.status = variant.status;
                variants.push(newServiceVariant);
            }
            await this.itemService.createServiceVariants(variants);
        } catch (e) {
            this.logger.debug('-------- ITEM SERVICE VARIANT SEED FAILED -----------');
        }
    }

    private async seedPartnerItemCategory(): Promise<void> {
        if (await this.itemPartnerCategoryService.canRunSeed()) {
            try {
                const categories: PartnerItemCategory[] = [];
                for (const seed of seedPartnerCategory) {
                    const item = new PartnerItemCategory();
                    item.uuid = await this.commonService.generateToken();
                    item.itemCategory = await this.itemCategoryService.getByIdentifier(
                        seed.itemCategoryEnum
                    );
                    item.settingPartner = await this.partnerService.getByIdentifier(
                        seed.settingPartnerEnum
                    );
                    item.provider = await this.itemProviderService.getProviderByTitle(
                        seed.providerEnum
                    );
                    item.status = seed.statusEnum;
                    item.partnerServiceId = seed.partnerServiceId;
                    categories.push(item);
                }
                const seed = await this.itemPartnerCategoryService.seedSettingPartnerItemCategories(
                    categories
                );
                if (seed) {
                    this.logger.debug(
                        '-------- PARTNER ITEM CATEGORIES SEEDED SUCCESSFULLY. -----------'
                    );
                }
            } catch (e) {
                this.logger.debug('-------- PARTNER ITEM CATEGORIES SEED FAILED -----------');
            }
        }
    }

    private async seedPartnerItemService(): Promise<void> {
        if (await this.itemPartnerServiceImpl.canRunSeed()) {
            try {
                const settings: PartnerItemService[] = [];
                for (const seed of seedPartnerServices) {
                    const setting: PartnerItemService = new PartnerItemService();
                    setting.uuid = await this.commonService.generateToken();
                    setting.itemCategory = await this.itemCategoryService.getByIdentifier(
                        seed.itemCategory
                    );
                    setting.settingPartner = await this.partnerService.getByIdentifier(
                        seed.settingPartner
                    );
                    setting.itemService = await this.itemService.getServiceBySlug(
                        seed.itemServiceSlug
                    );
                    setting.itemServiceVariant = await this.itemService.getServiceVariantBySlug(
                        seed.itemServiceVariantSlug
                    );
                    setting.status = StatusEnum.ACTIVE;
                    setting.partnerServiceId = seed.partnerServiceId;
                    setting.costPrice = seed.costPrice;
                    setting.sellingPrice = seed.sellingPrice;
                    setting.profitMarginPercent = seed.profitMarginPercent;
                    setting.profitAmount = seed.profitAmount;
                    setting.agentPrice = seed.agentPrice;
                    setting.regularPrice = seed.regularPrice;
                    settings.push(setting);
                }
                const seeded = await this.itemPartnerServiceImpl.seedSettingPartnerItemCategories(
                    settings
                );
                if (seeded) {
                    this.logger.debug(
                        '-------- PARTNER ITEM SERVICES SEEDED SUCCESSFULLY -----------'
                    );
                }
            } catch (e) {
                this.logger.debug('-------- PARTNER ITEM SERVICES SEEDED FAILED. -----------');
            }
        }
    }

    private async seedSuperAdminUser(): Promise<void> {
        if (await this.userService.canSeed()) {
            try {
                const superadmin: User = new User();
                superadmin.uuid = await this.commonService.generateToken();
                superadmin.firstName = seedSystemUser.firstName;
                superadmin.lastName = seedSystemUser.lastName;
                superadmin.email = seedSystemUser.email;
                superadmin.username = seedSystemUser.username;
                superadmin.salt = await this.commonService.generateSalt();
                superadmin.password = await this.commonService.hashPassword(
                    seedSystemUser.password,
                    superadmin.salt
                );
                superadmin.phone = seedSystemUser.phone;
                superadmin.isActive = seedSystemUser.isActive;
                superadmin.isVerified = seedSystemUser.isVerified;
                superadmin.type = UserTypeEnum.SUPER_ADMIN;
                const seeded = await this.userService.seedSystemUser(superadmin);
                if (seeded) {
                    this.logger.debug('-------- USER SEEDED SUCCESSFULLY  -----------');
                }
            } catch (e) {
                this.logger.debug('-------- USER SEEDED FAILED -----------');
            }
        }
    }
}
