import { SignInPayload, SignUpPayload } from '../../payloads/classes.payload';
import { User } from '../../../user/entity/user.entity';
import { IJwtToken } from '../../payloads/interface.payload';
import { UserTypeEnum } from '../../payloads/enums.payload';

export const mockSignInData: SignInPayload = {
    email: 'mock@email.com',
    password: 'password',
};

export const mockSignUpData: SignUpPayload = {
    // firstName: 'John',
    // lastName: 'Mock',
    phone: '0903982933',
    email: 'mock@email.com',
    password: 'password',
    username: 'user1234',
    // uuid: '',
};

const user = new User();
user.id = 1;
user.email = 'mock@email.com';
user.password = 'password';
export const mockUserData: User = user;

export const mockUsersData: User[] = [];
export const mockToken: string = 'f8b6a287-f302-4035-b34b-1ff2271d2481';

export const mockJwtToken: IJwtToken = {
    email: 'mock@email.com',
    token: mockToken,
    type: UserTypeEnum.REGULAR,
    uuid: mockToken,
};
