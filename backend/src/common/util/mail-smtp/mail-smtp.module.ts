import { Global, Module } from '@nestjs/common';
import { MailSmtpService } from './mail-smtp.service';
import { MailerModule, MailerOptions } from '@nestjs-modules/mailer';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ConfigService } from '@nestjs/config';

@Global() //make mail smtp module global
@Module({
	imports: [
		MailerModule.forRootAsync({
			imports: [],
			inject: [ConfigService],
			useFactory: async (config: ConfigService) =>
				({
					transport: {
						port: process.env.MAIL_PORT,
						host: process.env.MAIL_HOST,
						auth: {
							user: process.env.MAIL_USER,
							pass: process.env.MAIL_PASSWORD,
						},
						// logger: true,
						// debug: true,
						secure: false,
						opportunisticTLS: true,
						secureConnection: false,
					},
					defaults: { from: process.env.MAIL_FROM_EMAIL },
					template: {
						dir: join(__dirname, '../../email'),
						adapter: new HandlebarsAdapter(),
						options: {
							strict: true,
						},
					},
				} as MailerOptions),
		}),
	],
	providers: [MailSmtpService, ConfigService],
	exports: [MailSmtpService],
})
export class MailSmtpModule {}
