import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { IConfirmEmailData, ISiteProps } from '../../payloads/interface.payload';

@Injectable({})
export class MailSmtpService {
	constructor(private mailerService: MailerService) {}

	public async sendUserConfirmation(data: IConfirmEmailData) {
		const { userEmail, userLink, userName, siteProps } = data;

		return await this.mailerService.sendMail({
			to: userEmail,
			from: `"Support Team" <${siteProps.siteSupportEmail}>`, // override default from
			subject: `Email Verification - ${siteProps.siteName}.`,
			template: './verification', // `.hbs` extension is appended automatically
			context: {
				siteBaseUrl: siteProps.siteBaseUrl,
				siteContactUrl: siteProps.siteContactUrl,
				siteLogo: siteProps.siteLogoUrl,
				siteName: siteProps.siteName,
				siteEmail: siteProps.siteSupportEmail,
				siteAddress: siteProps.siteAddress,
				userName: userName,
				userLink: userLink,
				date: new Date().getFullYear(),
			},
		});
	}

	public async sendResetPasswordVerification(
		siteProps: ISiteProps,
		userName: string,
		userLink: string,
		userEmail: string,
	) {
		return await this.mailerService.sendMail({
			to: userEmail,
			from: `"Support Team" <${siteProps.siteSupportEmail}>`,
			subject: `Password Reset Verification`,
			template: './reset-password',
			context: {
				siteBaseUrl: siteProps.siteBaseUrl,
				siteContactUrl: siteProps.siteContactUrl,
				siteLogo: siteProps.siteLogoUrl,
				siteName: siteProps.siteName,
				siteEmail: siteProps.siteSupportEmail,
				siteAddress: siteProps.siteAddress,
				userName: userName,
				userLink: userLink,
				date: new Date().getFullYear(),
			},
		});
	}
}
