import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IJwtAuth } from '../payloads/interface.payload';
import { User } from '../../user/entity/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET,
        });
    }

    validate(payload: IJwtAuth): Promise<User> {
        const { email } = payload;
        const user = this.userRepository.findOne({ email: email });
        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
