import { Provider } from '@nestjs/common';
import * as MailJetLib from 'node-mailjet'

export const MailJet = 'lib:node-mailjet'

export const mailJetProvider: Provider = {
  provide: MailJet,
  useValue: MailJetLib,
}