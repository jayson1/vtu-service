import { Module } from '@nestjs/common';
import { mailJetProvider } from './mail-jet.provider';
import { MailJetService } from './mail-jet.service';

@Module({
	providers: [mailJetProvider, MailJetService],
	exports: [mailJetProvider, MailJetService],
})
export class MailJetModule {}
