import { Inject, Injectable, Logger } from '@nestjs/common';
import { MailJet } from './mail-jet.provider';

@Injectable()
export class MailJetService {
	private logger = new Logger('MailJetService');
	private request;
	private supportEmail = '';
	private websiteName = '';

	constructor(@Inject(MailJet) private mailjet) {
		this.onInitialize();
	}

	public sendEmailMailJetClient(data: {
		email: string;
		fullName: string;
		html: string;
		subject: string;
		textPart: string;
		type: string;
	}) {
		this.logger.log(`sending ${data.type} to ${data.email}`);
		this.request
			.post('send', { version: 'v3.1' })
			.request({
				Messages: [
					{
						From: {
							Email: `${this.supportEmail}`,
							Name: 'Support',
						},
						To: [
							{
								Email: `${data.email}`,
								Name: `${data.fullName}`,
							},
						],
						Subject: data.subject || 'Attention Required',
						TextPart: data.textPart || 'Your attention is required to continue',
						HTMLPart: data.html,
						CustomID: `${data.type || 'CustomID'}`,
					},
				],
			})
			.then((result) => {
				this.logger.verbose(`${data.type} email sent successfully to ${data.email}`);
			})
			.catch((err) => {
				this.logger.error(`could not send ${data.type} email to ${data.email}`);
				this.logger.error(err.statusCode);
			});
	}

	private onInitialize() {
		this.logger.log('Connecting to MailJet');

		this.request = this.mailjet.connect(
			process.env.MAILJET_API_KEY,
			process.env.MAILJET_SECRET_KEY,
		);
		this.supportEmail = process.env.APP_SUPPORT_EMAIL;
		this.websiteName = process.env.APP_WEBSITE_NAME;
		this.logger.log('MailJet Connection Successful');
	}
}
