import { Module } from '@nestjs/common';
import { CommonService } from './service/common.service';
import { MailJetModule } from './util/mail-jet/mail-jet.module';
import { SettingModule } from '../setting/setting.module';
import { ItemModule } from '../item/item.module';
import { TransactionModule } from '../transaction/transaction.module';
import { UserModule } from '../user/user.module';
import { WalletModule } from '../wallet/wallet.module';
import { AuthModule } from '../auth/auth.module';
import { SubscriptionModule } from '../subscription/subscription.module';
import { InitService } from './service/init.service';
import { PartnerModule } from '../partner/partner.module';

@Module({
    imports: [
        AuthModule,
        UserModule,
        SettingModule,
        ItemModule,
        TransactionModule,
        WalletModule,
        MailJetModule,
        SubscriptionModule,
        PartnerModule,
    ],
    providers: [CommonService, InitService],
    exports: [
        AuthModule,
        CommonService,
        MailJetModule,
        UserModule,
        SettingModule,
        ItemModule,
        TransactionModule,
        WalletModule,
        PartnerModule,
    ],
})
export class CommonModule {}
