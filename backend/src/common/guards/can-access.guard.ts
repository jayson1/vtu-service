import { CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';

export class CanAccessGuard implements CanActivate {
    canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const params = request.params;
        const id = params.userId || params.id;
        return this.validateRequest(request, id);
    }

    async validateRequest(request: any, id: any): Promise<boolean> {
        const user = await request.user;
        if (user.id == id || user.isAdmin) {
            return Promise.resolve(true);
        } else {
            throw new UnauthorizedException();
        }
    }
}
