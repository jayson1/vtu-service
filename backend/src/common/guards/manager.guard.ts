import { CanActivate, ExecutionContext, Logger, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { UserTypeEnum } from '../payloads/enums.payload';
import { User } from '../../user/entity/user.entity';

export class ManagerGuard implements CanActivate {
	private logger = new Logger(ManagerGuard.name);
	canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
		const request = context.switchToHttp().getRequest();
		return this.validateRequest(request);
	}

	validateRequest(request: any): Promise<boolean> {
		const user: User = request.user;
		if (user) {
			const res = user.type == UserTypeEnum.MANAGER;
			return Promise.resolve(res);
		} else {
			throw new UnauthorizedException();
		}
	}
}
