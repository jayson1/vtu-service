import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class SuperAdminGuard implements CanActivate {
	canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
		const request = context.switchToHttp().getRequest();
		return this.validateRequest(request);
	}

	validateRequest(request: any): Promise<boolean> {
		const user = request.user;
		if (user) {
			return user.isAdmin;
		} else {
			throw new UnauthorizedException();
		}
	}
}
