import { EntityRepository, getConnection, Repository } from 'typeorm';
import { PasswordReset } from '../entity/password-reset.entity';

@EntityRepository(PasswordReset)
export class PasswordResetRepository extends Repository<PasswordReset> {
    public async updateValueById(
        id: number,
        token: string,
        link: string,
        expiry: Date
    ): Promise<boolean> {
        const res = await getConnection()
            .createQueryBuilder()
            .update(PasswordReset)
            .set({ token: token, link: link, expiration: expiry, attempt: () => 'attempt + 1' })
            .where('id = :id', { id: id })
            .execute();
        return res.affected != 0;
    }

    public async deleteOneById(id: number) {
        return await getConnection()
            .createQueryBuilder()
            .delete()
            .from(PasswordReset)
            .where('id = :id', { id: id })
            .execute();
    }
}
