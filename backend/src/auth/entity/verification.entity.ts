import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';

@Entity({ name: TableNames.VERIFICATION_TBL })
export class Verification extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'email', type: 'varchar', length: 200, unique: true })
    email: string;

    @Column({ name: 'token', type: 'text', nullable: false })
    token: string;

    @Column({ name: 'user_uuid', type: 'text', nullable: false })
    userUuid: string;

    @CreateDateColumn({ name: 'created_at' })
    createdAt: Date;

    @Column({ name: 'expire_at', nullable: false })
    expiryAt: Date;
}
