import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';

@Entity({ name: TableNames.PASSWORD_RESET_TBL })
export class PasswordReset extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    email: string;

    @Column()
    token: string;

    @Column()
    link: string;

    @Column({ nullable: true, type: 'numeric', default: 0 })
    attempt: number;

    @Column()
    expiration: Date;

    @CreateDateColumn({ name: 'created_at' })
    createdAt: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt: Date;

    constructor(email?: string, token?: string, link?: string, expiration?: Date) {
        super();
        this.email = email;
        this.token = token;
        this.link = link;
        this.expiration = expiration;
    }
}
