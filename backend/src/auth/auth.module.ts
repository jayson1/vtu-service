import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthController } from './controller/auth.controller';
import { AuthService } from './service/auth.service';
import { UserModule } from '../user/user.module';
import { CommonModule } from '../common/common.module';
import { Verification } from './entity/verification.entity';
import { VerificationService } from './service/verification.service';
import { SettingModule } from '../setting/setting.module';
import { PasswordResetService } from './service/password-reset.service';
import { JwtStrategy } from './service/jwt.strategy';
import { PasswordResetRepository } from './respository/password-reset.repository';

@Module({
    imports: [
        TypeOrmModule.forFeature([Verification, PasswordResetRepository]),
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.registerAsync({
            inject: [ConfigService],
            useFactory: (config: ConfigService) => ({
                secret: process.env.JWT_SECRET,
                signOptions: { expiresIn: process.env.JWT_EXPIRES_IN },
            }),
        }),
        forwardRef(() => CommonModule),
    ],
    controllers: [AuthController],
    providers: [JwtStrategy, AuthService, VerificationService, PasswordResetService],
    exports: [JwtStrategy, PassportModule, AuthService, VerificationService, PasswordResetService],
})
export class AuthModule {}
