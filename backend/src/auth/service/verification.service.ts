import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Verification } from '../entity/verification.entity';
import { Repository } from 'typeorm';
import { EmailVerifyPayload } from '../../common/payloads/classes.payload';
import { ValidationConstant } from '../../common/constants/app.constants';

@Injectable({})
export class VerificationService {
    private LOGGER = new Logger(VerificationService.name);
    constructor(
        @InjectRepository(Verification)
        private verificationRepository: Repository<Verification>
    ) {}

    /**
     * Create or Update user verification obj
     * @param payload
     * @return Promise<Verification>
     */
    public async createOrUpdate(payload: EmailVerifyPayload): Promise<Verification> {
        const { email, token } = payload;
        try {
            if (await this.hasVerification(email)) {
                return await this.update(email, token);
            }
            return await this.create(payload);
        } catch (e) {
            this.LOGGER.error(e);
        }
    }

    public async update(email: string, token: string): Promise<Verification> {
        const verification: Verification = await this.findByEmail(email);
        verification.token = token;
        verification.expiryAt = this.getExpirationDate();
        return await this.verificationRepository.save(verification);
    }

    public async create(payload: EmailVerifyPayload): Promise<Verification> {
        const verification: Verification = new Verification();
        verification.email = payload.email;
        verification.token = payload.token;
        verification.userUuid = payload.uuid;
        verification.expiryAt = this.getExpirationDate();
        return await this.verificationRepository.save(verification);
    }

    /**
     * Check if user already has pending verification obj
     * @param email
     * @return Promise<bool>
     */
    public async hasVerification(email: string): Promise<boolean> {
        const verification = await this.verificationRepository.findOne({ email: email });
        return verification != undefined;
    }

    public async findById(id: string): Promise<Verification> {
        return await this.verificationRepository.findOneOrFail(id);
    }

    public async findByEmail(email: string): Promise<Verification> {
        const verification = await this.verificationRepository.findOne({ email: email });
        if (!verification) {
            throw new NotFoundException(
                `${ValidationConstant.NOT_FOUND_EXCEPTION} Verification for: ${email}`
            );
        }
        return verification;
    }

    public async findByUuid(uuid: string): Promise<Verification> {
        const verification = await this.verificationRepository.findOne({ userUuid: uuid });
        if (!verification) {
            throw new NotFoundException(ValidationConstant.NOT_FOUND_EXCEPTION);
        }
        return verification;
    }

    public getExpirationDate() {
        const date = new Date();
        // expire after one hour
        return new Date(date.getTime() + 1000 * 60 * 60);
    }

    public async removeOne(id: number): Promise<boolean> {
        const deleteResult = await this.verificationRepository.delete({ id: id });
        return deleteResult.affected != 0;
    }
}
