import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../../user/service/user.service';
import { IJwtAuth } from '../../common/payloads/interface.payload';
import { User } from '../../user/entity/user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private userService: UserService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET,
        });
    }

    public async validate(payload: IJwtAuth): Promise<User> {
        const { email } = payload;
        const user = await this.userService.findOneUuidByEmail(email);
        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
