import {
    BadRequestException,
    HttpException,
    HttpStatus,
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../../user/service/user.service';
import {
    EmailVerifyPayload,
    SignInPayload,
    SignUpPayload,
} from '../../common/payloads/classes.payload';
import { CommonService } from '../../common/service/common.service';
import { ValidationConstant } from '../../common/constants/app.constants';
import { IResponse, IJwtAuth, IJwtToken } from '../../common/payloads/interface.payload';
import { VerificationService } from './verification.service';
import { User } from '../../user/entity/user.entity';
import { PasswordResetService } from './password-reset.service';

@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private verificationService: VerificationService,
        private commonService: CommonService,
        private passwordResetService: PasswordResetService,
        private jwtService: JwtService
    ) {}

    public async signUpUser(payload: SignUpPayload): Promise<IResponse> {
        const saved = await this.userService.createOne(payload);
        this.sendVerificationEmail(saved).then();
        return Promise.resolve({
            message: ValidationConstant.SIGNUP_SUCCESS,
            statusCode: HttpStatus.OK,
        });
    }

    public async signInUser(signInPayload: SignInPayload): Promise<IJwtToken> {
        const user = await this.userService.findOneUuidByEmail(signInPayload.email);
        const isCorrectPassword: boolean = await this.userService.validateUserPassword(
            user,
            signInPayload.password
        );

        if (!isCorrectPassword) {
            throw new UnauthorizedException(ValidationConstant.INVALID_CREDENTIALS);
        }
        if (!user.isVerified) {
            this.reSendVerificationEmail(user.email).then((r) => console.log(r));
            throw new UnauthorizedException(ValidationConstant.ACCOUNT_NOT_VERIFIED);
        }
        if (!user.isActive) {
            throw new UnauthorizedException(ValidationConstant.ACCOUNT_DISABLED);
        }

        const payload: IJwtAuth = { email: user.email, uuid: user.uuid };
        const accessToken = this.jwtService.sign(payload);

        return {
            email: user.email,
            uuid: user.uuid,
            token: accessToken,
            type: user.type,
        };
    }

    /**
     * Verify user email account after registration
     * @param token
     * @param uuid
     */
    public async emailVerification(token: string, uuid: string): Promise<IResponse> {
        const userToken = await this.verificationService.findByUuid(uuid);
        if (userToken && userToken.token === token) {
            const isUserUpdated: boolean = await this.userService.updateUserIsVerified(uuid, true);
            const isTokenRemoved: boolean = await this.verificationService.removeOne(userToken.id);
            if (isUserUpdated && isTokenRemoved) {
                return { message: ValidationConstant.VERIFICATION_SENT, statusCode: HttpStatus.OK };
            } else {
                return {
                    message: ValidationConstant.ACTION_FAILED,
                    statusCode: HttpStatus.BAD_REQUEST,
                };
            }
        } else {
            throw new HttpException(ValidationConstant.INVALID_CREDENTIALS, HttpStatus.FORBIDDEN);
        }
    }

    public async resetPasswordVerification(email: string): Promise<IResponse> {
        const user = await this.userService.findOneByEmail(email);
        const savedReset = await this.passwordResetService.createOrUpdate(user);
        if (savedReset) {
            this.commonService
                .sendResetPasswordEmail(user.email, user.username, savedReset)
                .then((r) => console.log(r));
            return {
                statusCode: HttpStatus.OK,
                message: ValidationConstant.RESET_VERIFICATION_EMAIL_SENT,
            };
        }
        return { statusCode: HttpStatus.BAD_REQUEST, message: ValidationConstant.ACTION_FAILED };
    }

    public async resetPassword(email: string, newPassword: string): Promise<IResponse> {
        try {
            const user = await this.userService.findOneByEmail(email);
            await this.userService.updateUserPassword(user, newPassword);
            return { statusCode: HttpStatus.OK, message: ValidationConstant.UPDATED_SUCCESSFUL };
        } catch (e) {
            throw new HttpException(ValidationConstant.ACTION_FAILED, e);
        }
    }

    private async sendVerificationEmail(user: User) {
        const payload: EmailVerifyPayload = new EmailVerifyPayload();
        payload.uuid = user.uuid;
        payload.token = await this.commonService.generateToken();
        payload.email = user.email;
        await this.verificationService.createOrUpdate(payload);
        this.commonService
            .sendAccountVerificationEmail(user, payload.token)
            .then((r) => console.log(r));
    }

    public async verifyResetTokenAndEmail(token: string, email: string): Promise<IResponse> {
        const passwordReset = await this.passwordResetService.findOne(email);
        if (!passwordReset) {
            throw new BadRequestException(ValidationConstant.RESET_LINK_INVALID);
        } else if (passwordReset.token !== token) {
            throw new BadRequestException(ValidationConstant.RESET_LINK_INVALID);
        } else if (passwordReset && passwordReset.expiration < new Date()) {
            throw new BadRequestException(ValidationConstant.RESET_LINK_EXPIRED);
        }
        await this.passwordResetService.removeOne(passwordReset.id).then((r) => console.log(r));
        return { message: ValidationConstant.RESET_LINK_VALID, statusCode: HttpStatus.OK };
    }

    private async reSendVerificationEmail(email: string): Promise<IResponse> {
        const user = await this.userService.findOneByUuid(email);
        const checkEmail = await this.verificationService.findByEmail(email);
        if (!checkEmail) {
            throw new UnauthorizedException(ValidationConstant.NO_PENDING_VERIFICATION);
        }
        this.commonService
            .sendAccountVerificationEmail(user, checkEmail.token)
            .then((r) => console.log(r));
        return { message: ValidationConstant.VERIFICATION_SENT, statusCode: HttpStatus.OK };
    }
}
