import {
	Injectable,
	InternalServerErrorException,
	Logger,
	NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PasswordReset } from '../entity/password-reset.entity';
import { User } from '../../user/entity/user.entity';
import { CommonService } from '../../common/service/common.service';
import { PasswordResetRepository } from '../respository/password-reset.repository';
import { ValidationConstant } from '../../common/constants/app.constants';

@Injectable()
export class PasswordResetService {
	private LOGGER = new Logger(PasswordResetService.name);
	constructor(
		@InjectRepository(PasswordResetRepository)
		private passwordResetRepository: PasswordResetRepository,
		private commonService: CommonService,
	) {}

	public async createOrUpdate(user: User): Promise<PasswordReset> {
		let existingPasswordReset = await this.hasResetRecord(user.email);
		if (!existingPasswordReset) {
			return await this.createPasswordReset(user.email);
		}
		return this.updatePasswordReset(existingPasswordReset, user.email);
	}

	public async createPasswordReset(email: string): Promise<PasswordReset> {
		const token = await this.commonService.generateToken();
		const link = await this.commonService.generateResetPasswordLink(email, token);
		try {
			let payload: PasswordReset = new PasswordReset();
			payload.email = email;
			payload.attempt = 0;
			payload.link = link;
			payload.token = token;
			payload.expiration = this.commonService.generateExpirationDate();
			return await this.passwordResetRepository.save(payload);
		} catch (e) {
			this.LOGGER.error(e);
			throw new InternalServerErrorException(e.message);
		}
	}

	public async updatePasswordReset(reset: PasswordReset, email: string): Promise<PasswordReset> {
		try {
			const token = await this.commonService.generateToken();
			reset.attempt = Number(reset.attempt + 1);
			reset.token = token;
			reset.link = await this.commonService.generateResetPasswordLink(email, token);
			reset.expiration = this.commonService.generateExpirationDate();
			return await this.passwordResetRepository.save(reset);
		} catch (e) {
			this.LOGGER.error(e);
			throw new InternalServerErrorException(e.message);
		}
	}

	public async hasResetRecord(email: string): Promise<PasswordReset> {
		let reset = await this.passwordResetRepository.findOne({ email });
		if (!reset) {
			return Promise.resolve(null);
		}
		return reset;
	}

	public async findOne(email: string): Promise<PasswordReset> {
		const reset = await this.passwordResetRepository.findOne({ email });
		if (!reset) throw new NotFoundException(ValidationConstant.NOT_FOUND_EXCEPTION);
		return reset;
	}

	public async removeOne(id: number): Promise<boolean> {
		let deleteResult = await this.passwordResetRepository.deleteOneById(id);
		return deleteResult.affected != 0;
	}
}
