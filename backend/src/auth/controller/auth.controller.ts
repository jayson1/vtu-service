import { Body, Controller, Get, Param, Post, ValidationPipe } from '@nestjs/common';
import { ApiRoutes } from '../../common/constants/api.routes';
import { AuthService } from '../service/auth.service';
import { SignInPayload, SignUpPayload } from '../../common/payloads/classes.payload';
import { IResponse, IJwtToken } from '../../common/payloads/interface.payload';

@Controller(ApiRoutes.AUTH_ROUTE)
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post(ApiRoutes.AUTH_SIGN_UP)
    public async signUp(@Body(ValidationPipe) payload: SignUpPayload): Promise<IResponse> {
        return this.authService.signUpUser(payload);
    }

    @Post(ApiRoutes.AUTH_EMAIL_VERIFY)
    public async verifyEmailAndToken(
        @Body() payload: { token: string; uuid: string }
    ): Promise<IResponse> {
        return await this.authService.emailVerification(payload.token, payload.uuid);
    }

    @Post(ApiRoutes.AUTH_SIGN_IN)
    public async signIn(@Body(ValidationPipe) payload: SignInPayload): Promise<IJwtToken> {
        return await this.authService.signInUser(payload);
    }

    @Get(ApiRoutes.AUTH_SEND_RESET_PASSWORD_EMAIL)
    public async resetPasswordVerification(@Param('email') email: string): Promise<IResponse> {
        return await this.authService.resetPasswordVerification(email);
    }

    @Get(ApiRoutes.AUTH_RESET_TOKEN_VERIFY)
    async verifyResetPasswordTokenAndEmail(
        @Param('token') token: string,
        @Param('email') email: string
    ): Promise<IResponse> {
        return await this.authService.verifyResetTokenAndEmail(token, email);
    }

    @Post(ApiRoutes.AUTH_RESET_PASSWORD)
    public async resetPasswordAfterVerification(
        @Body(ValidationPipe) payload: SignInPayload
    ): Promise<IResponse> {
        return this.authService.resetPassword(payload.email, payload.password);
    }
}
