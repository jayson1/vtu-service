import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanRepository } from '../repository/plan.repository';
import { CommonService } from '../../common/service/common.service';
import { Plan } from '../entity/plan.entity';

@Injectable()
export class PlanService {
    private logger = new Logger(PlanService.name);
    constructor(
        @InjectRepository(PlanRepository)
        private planRepository: PlanRepository,
        private commonService: CommonService
    ) {}

    public async getPlanByRank(rank: number): Promise<Plan> {
        return await this.planRepository.findOneOrFail({ hierarchy: rank });
    }

    public async seedPlans(plans: Plan[]): Promise<boolean> {
        if (plans.length) {
            await this.planRepository.createMany(plans);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    public async canRunSeeder(): Promise<boolean> {
        const count = await this.planRepository.countPlan();
        return count < 1;
    }
}
