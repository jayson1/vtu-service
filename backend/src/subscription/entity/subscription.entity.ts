import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import { Plan } from './plan.entity';
import { User } from '../../user/entity/user.entity';
import { StatusEnum } from '../../common/payloads/enums.payload';

@Entity({ name: TableNames.SUBSCRIPTION_TBL })
export class Subscription {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @OneToOne(() => Plan, { nullable: false })
    @JoinColumn({ name: 'plan_id' })
    plan: Plan;

    @Column({ type: 'date', name: 'start_date' })
    start_date: Date;

    @Column({ type: 'date', name: 'end_date' })
    end_date: Date;

    @Column({ type: 'integer', name: 'number_of_days' })
    number_of_days: number;

    @Column({ type: 'integer', name: 'number_of_months' })
    number_of_months: number;

    @Column({ type: 'date', name: 'cancel_on_date' })
    cancel_on_date: Date;

    @Column({ type: 'date', name: 'cancelled_on_date' })
    cancelled_on_date: Date;

    @OneToOne(() => User, { nullable: true })
    @JoinColumn({ name: 'user_id' })
    user: User;

    @Column({ type: 'text', name: 'status', nullable: false })
    status: StatusEnum;

    @Column({ type: 'boolean', name: 'is_recurring' })
    isRecurring: boolean;
}
