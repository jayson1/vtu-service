import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import { Plan } from './plan.entity';

@Entity({ name: TableNames.SUBSCRIPTION_HISTORY_TBL })
export class SubscriptionHistory {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @OneToOne(() => Plan)
    @JoinColumn({ name: 'prev_plan_id' })
    prevPlan: Plan;

    @OneToOne(() => Plan)
    @JoinColumn({ name: 'new_plan_id' })
    newPlan: Plan;

    @Column({ type: 'date', name: 'prev_start_date' })
    prevStartDate: Date;

    @Column({ type: 'date', name: 'prev_end_date' })
    prevEndDate: Date;

    @Column({ type: 'date', name: 'new_start_date' })
    newStartDate: Date;

    @Column({ type: 'date', name: 'new_end_date' })
    newEndDate: Date;

    @Column({ type: 'text', name: 'payment_channel' })
    paymentChannel: string;
}
