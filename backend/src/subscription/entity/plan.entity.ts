import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import { Setting } from '../../setting/entity/setting.entity';

@Entity({ name: TableNames.PLAN_TBL })
export class Plan {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @Column({ name: 'title', type: 'text', nullable: false })
    title: string;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'price_per_day', nullable: false })
    pricePerDay: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'price_per_month', nullable: false })
    pricePerMonth: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'price_per_year', nullable: false })
    pricePerYear: number;

    @Column({ type: 'integer', name: 'hierarchy', nullable: false })
    hierarchy: number;

    @OneToMany(() => Setting, (settings) => settings.plan)
    settings: Setting[];
}
