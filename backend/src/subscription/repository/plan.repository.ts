import { EntityRepository, Repository } from 'typeorm';
import { Plan } from '../entity/plan.entity';

@EntityRepository(Plan)
export class PlanRepository extends Repository<Plan> {
    public async createOne(plan: Plan): Promise<Plan> {
        return await this.save(plan);
    }

    public async createMany(plans: Plan[]): Promise<void> {
        if (plans.length) {
            await this.save(plans);
        }
    }

    public async getOneByUid(uid: string): Promise<Plan> {
        return await this.findOneOrFail({ uuid: uid });
    }

    public async getOneById(id: number): Promise<Plan> {
        return await this.findOneOrFail({ id: id });
    }

    public async countPlan(): Promise<number> {
        return await this.count();
    }
}
