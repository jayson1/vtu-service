import { EntityRepository, Repository } from 'typeorm';
import { SubscriptionHistory } from '../entity/subscription-history.entity';

@EntityRepository(SubscriptionHistory)
export class SubscriptionHistoryRepository extends Repository<SubscriptionHistory> {}
