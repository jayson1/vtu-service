import { EntityRepository, Repository } from 'typeorm';
import { Subscription } from '../entity/subscription.entity';

@EntityRepository(Subscription)
export class SubscriptionRepository extends Repository<Subscription> {}
