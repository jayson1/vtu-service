import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from '../common/common.module';
import { PlanService } from './service/plan.service';
import { PlanRepository } from './repository/plan.repository';
import { SubscriptionRepository } from './repository/subscription.repository';
import { SubscriptionHistoryRepository } from './repository/subscription-history.repository';

@Module({
    imports: [
        forwardRef(() => CommonModule),
        TypeOrmModule.forFeature([
            PlanRepository,
            SubscriptionRepository,
            SubscriptionHistoryRepository,
        ]),
    ],
    controllers: [],
    providers: [PlanService],
    exports: [PlanService],
})
export class SubscriptionModule {}
