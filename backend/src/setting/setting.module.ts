import { forwardRef, Module } from '@nestjs/common';
import { SettingController } from './controller/setting.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingService } from './service/setting.service';
import { SettingRepository } from './repository/setting.repository';
import { PartnerRepository } from '../partner/repository/partner.repository';
import { CommonModule } from '../common/common.module';
import { PartnerService } from '../partner/service/partner.service';
import { PartnerItemCategoryService } from '../partner/service/partner-item-category.service';
import { PartnerItemCategoryRepository } from '../partner/repository/partner-item-category.repository';
import { PartnerItemServiceRepository } from '../partner/repository/partner-item-service.repository';
import { PartnerItemServiceImplService } from '../partner/service/partner-item-service-impl.service';

@Module({
    controllers: [SettingController],
    imports: [
        TypeOrmModule.forFeature([
            SettingRepository,
            PartnerRepository,
            PartnerItemCategoryRepository,
            PartnerItemServiceRepository,
        ]),
        forwardRef(() => CommonModule),
    ],
    providers: [
        SettingService,
        PartnerService,
        PartnerItemCategoryService,
        PartnerItemServiceImplService,
    ],
    exports: [
        SettingService,
        PartnerService,
        PartnerItemCategoryService,
        PartnerItemServiceImplService,
    ],
})
export class SettingModule {}
