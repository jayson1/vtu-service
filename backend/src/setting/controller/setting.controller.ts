import { Body, Controller, Get, Post, ValidationPipe } from '@nestjs/common';
import { ApiRoutes } from '../../common/constants/api.routes';
import { IResponse } from '../../common/payloads/interface.payload';
import { SettingService } from '../service/setting.service';
import { SettingPayload } from '../../common/payloads/classes.payload';

@Controller(ApiRoutes.SETTING_ROUTE)
export class SettingController {
    constructor(private settingService: SettingService) {}

    // @Get(ApiRoutes.SETTING_SETUP)
    // public async setUpSettings(): Promise<IResponse> {
    // 	return this.settingService.setUpSettingKeys();
    // }

    @Post(ApiRoutes.SETTING_UPDATE)
    public async updateSetting(
        @Body(ValidationPipe) payload: SettingPayload[]
    ): Promise<IResponse> {
        return this.settingService.updateMany(payload);
    }
}
