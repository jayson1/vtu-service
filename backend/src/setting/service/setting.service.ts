import { HttpStatus, Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IResponse } from '../../common/payloads/interface.payload';
import { ValidationConstant } from '../../common/constants/app.constants';
import { SettingPayload } from '../../common/payloads/classes.payload';
import { SettingRepository } from '../repository/setting.repository';
import { Setting } from '../entity/setting.entity';
import { Plan } from '../../subscription/entity/plan.entity';
import { SettingSectionEnum } from '../../common/payloads/enums.payload';

@Injectable({})
export class SettingService {
    private logger = new Logger(SettingService.name);

    constructor(
        @InjectRepository(SettingRepository) private settingRepository: SettingRepository
    ) {}

    public async updateMany(payloads: SettingPayload[]): Promise<IResponse> {
        try {
            if (payloads.length > 0) {
                for (const payload of payloads) {
                    await this.settingRepository.updateValueByKey(payload.key, payload.value);
                }
                return {
                    message: ValidationConstant.UPDATED_SUCCESSFUL,
                    statusCode: HttpStatus.OK,
                };
            }
            return {
                message: ValidationConstant.CANNOT_UPDATE_EMPTY,
                statusCode: HttpStatus.BAD_REQUEST,
            };
        } catch (e) {
            throw new InternalServerErrorException(e);
        }
    }

    public async findByKey(key: string): Promise<Setting> {
        return await this.settingRepository.findOneOrFail({ key: key });
    }

    public async findAllBySection(section: SettingSectionEnum): Promise<Setting[]> {
        return await this.settingRepository.find({ section: section });
    }

    public async findAllByPlan(plan: Plan): Promise<Setting[]> {
        return await this.settingRepository.find({ plan: plan });
    }

    /* seed default setting data */
    public async seedSettings(settings: Setting[]): Promise<boolean> {
        if (settings.length) {
            await this.settingRepository.createMany(settings);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    /* check if database has record */
    public async canRunSeeder(): Promise<boolean> {
        const count = await this.settingRepository.count();
        return count < 1;
    }
}
