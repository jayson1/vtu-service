import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SettingSectionEnum, SettingValueTypeEnum } from '../../common/payloads/enums.payload';
import { TableNames } from '../../common/constants/table-names.constants';
import { Plan } from '../../subscription/entity/plan.entity';

@Entity({ name: TableNames.SETTING_TBL })
export class Setting {
    /*
    this model is kind of like global settings, (what the user can change depends on
    what plan they have subscribed to. this model keep records for the all settings and config properties.
    (ie. a user can toggle or disable certain service category if they currently don't want their customers to purchase,
    or service is temporarily down )
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @Column({ type: 'varchar', length: 200, unique: true, name: 'setting_key', nullable: false })
    key: string;

    @Column({ type: 'text', name: 'setting_value', nullable: true })
    value: string;

    @Column({ type: 'text', name: 'setting_section', nullable: false })
    section: SettingSectionEnum;

    @ManyToOne(() => Plan, { nullable: false })
    @JoinColumn({ name: 'plan_id' })
    plan: Plan;

    @Column({ type: 'text', name: 'type', nullable: false })
    type: SettingValueTypeEnum;

    constructor(key?: string, section?: SettingSectionEnum) {
        this.key = key;
        this.section = section;
    }
}
