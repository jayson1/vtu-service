export class SettingNotification {
    // notification that pop ups on website
    shortTitle: string;
    datePublished: Date;
    timePublished: string;
    featuredImageUrl: string;
    body: string;
    repeatCount: number; // number of times to repeat notifications
    showOnPageLoad: boolean;
    targetSpecificPages: string;
    showOnAllPage: boolean;
    stopAfterClose: boolean;
    //
}
