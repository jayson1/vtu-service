import { EntityRepository, getConnection, Repository } from 'typeorm';
import { Setting } from '../entity/setting.entity';
import { SettingKeyEnum } from '../../common/payloads/enums.payload';
import { InternalServerErrorException, Logger } from '@nestjs/common';

@EntityRepository(Setting)
export class SettingRepository extends Repository<Setting> {
    private logger = new Logger(SettingRepository.name);

    public async updateValueByKey(key: SettingKeyEnum, value: any): Promise<boolean> {
        try {
            const res = await getConnection()
                .createQueryBuilder()
                .update(Setting)
                .set({ value: value })
                .where('key = :key', { key: key })
                .execute();
            return res.affected != 0;
        } catch (e) {
            this.logger.error(e.message, e.stack);
            return Promise.resolve(false);
        }
    }

    public async createMany(setting: Setting[]): Promise<void> {
        try {
            if (setting.length) {
                await this.save(setting);
            }
        } catch (e) {
            this.logger.error(e.message, e.stack);
            throw new InternalServerErrorException(e);
        }
    }
}
