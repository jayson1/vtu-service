import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import { ItemCategory } from '../../item/entity/item-category.entity';
import { Partner } from './partner.entity';
import { StatusEnum } from '../../common/payloads/enums.payload';
import { ItemProvider } from '../../item/entity/item-provider.entity';

@Entity({ name: TableNames.PARTNER_CATEGORY_TBL })
export class PartnerItemCategory {
    /*
    service partner: the external api provider that we handshake to pay for service
    (ie. if customer is buying data, we connect with partner
    and perform the transaction using provider api eg: vt pass
    this model hold the setting for which provider is responsible to handle with service category
    (ie. VT Pass will handle electricity, Alex Data will handle data payment
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @ManyToOne(() => ItemCategory)
    @JoinColumn({ name: 'item_category_id' })
    itemCategory: ItemCategory;

    @ManyToOne(() => Partner)
    @JoinColumn({ name: 'setting_partner_id' })
    settingPartner: Partner;

    @ManyToOne(() => ItemProvider)
    @JoinColumn({ name: 'item_provider_id' })
    provider: ItemProvider;

    @Column({ type: 'text', name: 'status' })
    status: StatusEnum = StatusEnum.ACTIVE;

    @Column({ type: 'text', name: 'partner_service_id' })
    partnerServiceId: string; // id from partner on each item category (ie. if category is data, => NetworkId on partners website)

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;
}
