import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import { ItemCategory } from '../../item/entity/item-category.entity';
import { Partner } from './partner.entity';
import { ScalePriceDirectionEnum, StatusEnum } from '../../common/payloads/enums.payload';
import { ItemService } from '../../item/entity/item-service.entity';
import { ItemServiceVariant } from '../../item/entity/item-service-variant.entity';

@Entity({ name: TableNames.PARTNER_SERVICE_TBL })
export class PartnerItemService {
    /*
    setting partner item service map properties related to specific partner and their services
    (ie. if we activate alex data as our active partner for data categories, we can keep
    specific properties such as dataId, costPrice, etc for alex data => used for making api request)
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

    @ManyToOne(() => ItemCategory)
    @JoinColumn({ name: 'item_category_id' })
    itemCategory: ItemCategory;

    @ManyToOne(() => Partner)
    @JoinColumn({ name: 'setting_partner_id' })
    settingPartner: Partner;

    @ManyToOne(() => ItemService)
    @JoinColumn({ name: 'item_service_id' })
    itemService: ItemService;

    @ManyToOne(() => ItemServiceVariant)
    itemServiceVariant: ItemServiceVariant;

    @Column({ type: 'text', name: 'status' })
    status: StatusEnum = StatusEnum.ACTIVE;

    @Column({ type: 'text', name: 'partner_service_id', nullable: false })
    partnerServiceId: string; // id from partner on each item category (ie. serviceId on partner's website.)

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'cost_price' })
    costPrice: number = 0;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'selling_price' })
    sellingPrice: number = 0;

    @Column({ type: 'integer', name: 'profit_margin_percent' })
    profitMarginPercent: number = 0;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'profit_amount' })
    profitAmount: number = 0;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'agent_price' })
    agentPrice: number = 0;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'regular_price' })
    regularPrice: number = 0;

    /* user can scale the prices of service by a certain
     markup for a specific partner
     */
    @Column({ type: 'integer', name: 'scale_price_markup' })
    scalePriceByMarkup: number = 0;

    /* scale price by amount */
    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'scale_price_amount' })
    scalePriceAmount: number = 0;

    @Column({ type: 'text', name: 'scale_direction', nullable: true })
    scaleDirection: ScalePriceDirectionEnum;
}
