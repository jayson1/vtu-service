import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import {
    ServicePartnerEnum,
    ServicePartnerAuthTypeEnum,
} from '../../common/payloads/enums.payload';
import { IServicePartnerEndpoint } from '../../common/payloads/interface.payload';

@Entity({ name: TableNames.PARTNER_TBL })
export class Partner {
    /*
    this model hold records for partners that will be responsible for handling service
    purchase. (ie. who are we going to buy the service from, Eg: Vt Pass, Alex Data, MegaSub etc)
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'boolean', name: 'is_deleted' })
    isDeleted: boolean = false;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @Column({ type: 'text', name: 'title' })
    title: string;

    @Column({ type: 'text', name: 'description', nullable: true })
    description: string;

    @Column({ type: 'text', name: 'website_url', nullable: true })
    websiteUrl: string;

    @Column({ type: 'text', name: 'live_base_url', nullable: true })
    liveBaseUrl: string;

    @Column({ type: 'simple-json', name: 'service_end_point' })
    serviceEndpoint: IServicePartnerEndpoint;

    @Column({ type: 'text', name: 'auth_username', nullable: true })
    authUsername: string;

    @Column({ type: 'text', name: 'auth_password', nullable: true })
    authPassword: string;

    @Column({ type: 'text', name: 'auth_token', nullable: true })
    authToken: string;

    @Column({ type: 'text', name: 'auth_type' })
    authType: ServicePartnerAuthTypeEnum;

    @Column({ type: 'text', name: 'identifier' })
    identifier: ServicePartnerEnum;

    @Column({ type: 'text', name: 'public_key', nullable: true })
    publicKey: string;

    @Column({ type: 'text', name: 'secret_key', nullable: true })
    secretKey: string;
}
