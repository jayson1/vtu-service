import { Controller } from '@nestjs/common';
import { ApiRoutes } from '../../common/constants/api.routes';
import { PartnerService } from '../service/partner.service';

@Controller(ApiRoutes.PARTNER_ROUTE)
export class PartnerController {
    constructor(private partnerService: PartnerService) {}
}
