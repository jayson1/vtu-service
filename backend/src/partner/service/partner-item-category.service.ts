import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PartnerItemCategoryRepository } from '../repository/partner-item-category.repository';
import { PartnerItemCategory } from '../entity/partner-item-category.entity';

@Injectable({})
export class PartnerItemCategoryService {
    private logger = new Logger(PartnerItemCategoryService.name);

    constructor(
        @InjectRepository(PartnerItemCategoryRepository)
        private partnerItemCategoryRepository: PartnerItemCategoryRepository
    ) {}

    public async seedSettingPartnerItemCategories(
        settingPartnersItemCategories: PartnerItemCategory[]
    ): Promise<boolean> {
        try {
            if (settingPartnersItemCategories.length) {
                await this.partnerItemCategoryRepository.save(settingPartnersItemCategories);
                return Promise.resolve(true);
            }
            return Promise.resolve(false);
        } catch (e) {
            this.logger.error(e);
        }
    }

    /* check if database has any record */
    public async canRunSeed(): Promise<boolean> {
        const count = await this.partnerItemCategoryRepository.count();
        return count < 1;
    }
}
