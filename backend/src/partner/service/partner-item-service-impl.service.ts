import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PartnerItemServiceRepository } from '../repository/partner-item-service.repository';
import { PartnerItemService } from '../entity/partner-item-service.entity';

@Injectable({})
export class PartnerItemServiceImplService {
    private logger = new Logger(PartnerItemServiceImplService.name);

    constructor(
        @InjectRepository(PartnerItemServiceRepository)
        private partnerItemServiceRepository: PartnerItemServiceRepository
    ) {}

    public async seedSettingPartnerItemCategories(
        partnerItemServices: PartnerItemService[]
    ): Promise<boolean> {
        if (partnerItemServices.length) {
            await this.partnerItemServiceRepository.save(partnerItemServices);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    /* check if database has any record */
    public async canRunSeed(): Promise<boolean> {
        const count = await this.partnerItemServiceRepository.count();
        return count < 1;
    }
}
