import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PartnerRepository } from '../repository/partner.repository';
import { Partner } from '../entity/partner.entity';
import { ServicePartnerEnum } from '../../common/payloads/enums.payload';

@Injectable({})
export class PartnerService {
    private logger = new Logger(PartnerService.name);

    constructor(
        @InjectRepository(PartnerRepository)
        private settingPartnerRepository: PartnerRepository
    ) {}

    public async getByIdentifier(identifier: ServicePartnerEnum): Promise<Partner> {
        return this.settingPartnerRepository.findOneOrFail({ identifier });
    }

    public async seedSettingPartner(settingPartners: Partner[]): Promise<boolean> {
        if (settingPartners.length) {
            await this.settingPartnerRepository.createMany(settingPartners);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    /* check if database has any record */
    public async canRunSeeder(): Promise<boolean> {
        const count = await this.settingPartnerRepository.count();
        return count < 1;
    }
}
