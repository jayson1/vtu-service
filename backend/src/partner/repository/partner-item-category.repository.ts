import { EntityRepository, Repository } from 'typeorm';
import { Logger } from '@nestjs/common';
import { PartnerItemCategory } from '../entity/partner-item-category.entity';

@EntityRepository(PartnerItemCategory)
export class PartnerItemCategoryRepository extends Repository<PartnerItemCategory> {
    private logger = new Logger(PartnerItemCategoryRepository.name);
}
