import { EntityRepository, Repository } from 'typeorm';
import { Partner } from '../entity/partner.entity';
import { Logger } from '@nestjs/common';

@EntityRepository(Partner)
export class PartnerRepository extends Repository<Partner> {
    private logger = new Logger(PartnerRepository.name);

    public async createMany(settingPartners: Partner[]): Promise<void> {
        try {
            if (settingPartners.length) {
                await this.save(settingPartners);
            }
        } catch (e) {
            this.logger.error(e);
        }
    }
}
