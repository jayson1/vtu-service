import { EntityRepository, Repository } from 'typeorm';
import { Logger } from '@nestjs/common';
import { PartnerItemService } from '../entity/partner-item-service.entity';

@EntityRepository(PartnerItemService)
export class PartnerItemServiceRepository extends Repository<PartnerItemService> {
    private logger = new Logger(PartnerItemServiceRepository.name);
}
