import { forwardRef, Module } from '@nestjs/common';
import { PartnerController } from './controller/partner.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from '../common/common.module';
import { PartnerRepository } from './repository/partner.repository';
import { PartnerItemCategoryRepository } from './repository/partner-item-category.repository';
import { PartnerItemServiceRepository } from './repository/partner-item-service.repository';
import { PartnerService } from './service/partner.service';
import { PartnerItemCategoryService } from './service/partner-item-category.service';
import { PartnerItemServiceImplService } from './service/partner-item-service-impl.service';

@Module({
    controllers: [PartnerController],
    imports: [
        TypeOrmModule.forFeature([
            PartnerRepository,
            PartnerItemCategoryRepository,
            PartnerItemServiceRepository,
        ]),
        forwardRef(() => CommonModule),
    ],
    providers: [PartnerService, PartnerItemCategoryService, PartnerItemServiceImplService],
    exports: [PartnerService, PartnerItemCategoryService, PartnerItemServiceImplService],
})
export class PartnerModule {}
