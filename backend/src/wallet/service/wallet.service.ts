import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WalletRepository } from '../repository/wallet.repository';
import { WalletPayload } from '../../common/payloads/classes.payload';
import { Wallet } from '../entity/wallet.entity';
import { CommonService } from '../../common/service/common.service';

@Injectable()
export class WalletService {
    constructor(
        @InjectRepository(WalletRepository)
        private walletRepository: WalletRepository,
        private commonService: CommonService
    ) {}

    public async createWallet(walletPayload: WalletPayload): Promise<Wallet> {
        try {
            const wallet: Wallet = new Wallet();
            wallet.uuid = await this.commonService.generateToken();
            wallet.user = walletPayload.user;
            wallet.balance = walletPayload.balance;
            return await this.walletRepository.save(wallet);
        } catch (e) {
            throw new InternalServerErrorException(e);
        }
    }
}
