import { forwardRef, Module } from '@nestjs/common';
import { WalletController } from './controller/wallet.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletRepository } from './repository/wallet.repository';
import { WalletHistoryRepository } from './repository/wallet-history.repository';
import { WalletService } from './service/wallet.service';
import { CommonModule } from '../common/common.module';

@Module({
    controllers: [WalletController],
    imports: [
        TypeOrmModule.forFeature([WalletRepository, WalletHistoryRepository]),
        forwardRef(() => CommonModule),
    ],
    providers: [WalletService],
    exports: [WalletService],
})
export class WalletModule {}
