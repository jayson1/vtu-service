import {
    BaseEntity,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { User } from '../../user/entity/user.entity';
import { TableNames } from '../../common/constants/table-names.constants';
import { WalletHistory } from './wallet-history.entity';

@Entity({ name: TableNames.WALLET_TBL })
export class Wallet extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', name: 'uuid', nullable: false, unique: true, length: 200 })
    uuid: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

    @OneToOne(() => User, (user) => user.wallet, { nullable: false })
    @JoinColumn({ name: 'user_id' })
    user: User;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'balance' })
    balance: number;

    @OneToMany(() => WalletHistory, (history) => history.wallet)
    histories: WalletHistory[];
}
