import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import { Wallet } from './wallet.entity';
import { Transaction } from '../../transaction/entity/transaction.entity';

@Entity({ name: TableNames.WALLET_HISTORY_TBL })
export class WalletHistory {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text', name: 'uuid' })
    uuid: string;

    @ManyToOne(() => Wallet, (wallet) => wallet.histories, { nullable: true })
    @JoinColumn({ name: 'wallet_id' })
    wallet: Wallet;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'amount_before' })
    amountBefore: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'amount_after' })
    amountAfter: number;

    @OneToOne(() => Transaction)
    @JoinColumn({ name: 'transaction_id' })
    transaction: Transaction;

    @Column({ type: 'date', name: 'date' })
    date: Date;
}
