import { EntityRepository, Repository } from 'typeorm';
import { WalletHistory } from '../entity/wallet-history.entity';

@EntityRepository(WalletHistory)
export class WalletHistoryRepository extends Repository<WalletHistory> {}
