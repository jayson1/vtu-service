import { EntityRepository, Repository } from 'typeorm';
import { Wallet } from '../entity/wallet.entity';

@EntityRepository(Wallet)
export class WalletRepository extends Repository<Wallet> {}
