import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { ApiRoutes } from './common/constants/api.routes';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const logger = new Logger('bootstrap');
    const port = process.env.APP_PORT || 3002;

    await app.setGlobalPrefix(ApiRoutes.API_PREFIX);
    await app.listen(port);

    logger.log(`${process.env.APP_WEBSITE_NAME} listening on port: ${port}`);
}
bootstrap().then();
