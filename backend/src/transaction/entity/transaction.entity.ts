import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNames } from '../../common/constants/table-names.constants';
import {
    TransactionCategoryEnum,
    TransactionStatusEnum,
    TransactionTypeEnum,
} from '../../common/payloads/enums.payload';
import { User } from '../../user/entity/user.entity';
import { ItemService } from '../../item/entity/item-service.entity';

@Entity({ name: TableNames.TRANSACTION_TBL })
export class Transaction {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text', name: 'uuid' })
    uuid: string;

    @Column({ type: 'text', name: 'status' })
    transactionStatus: TransactionStatusEnum;

    @Column({ type: 'date', name: 'date' })
    date: string;

    @Column({ type: 'time', name: 'time' })
    time: string;

    @Column({ type: 'text', name: 'description' })
    description: string;

    @Column({ type: 'text', name: 'type' })
    type: TransactionTypeEnum;

    @Column({ type: 'text', name: 'reference' })
    reference: string;

    @Column({ type: 'boolean', name: 'is_automated' })
    isAutomated: boolean;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'amount' })
    amount: number;

    @Column({ type: 'text', name: 'category' })
    category: TransactionCategoryEnum;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'commission_earn' })
    commissionEarn: number;

    @ManyToOne(() => User, (user) => user.transactions, { nullable: true })
    @JoinColumn({ name: 'user_id' })
    user: User;

    itemService: ItemService;
}
