import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionRepository } from './repository/transaction.repository';
import { TransactionService } from './service/transaction.service';
import { CommonModule } from '../common/common.module';

@Module({
    controllers: [],
    imports: [TypeOrmModule.forFeature([TransactionRepository]), forwardRef(() => CommonModule)],
    providers: [TransactionService],
    exports: [TransactionService],
})
export class TransactionModule {}
