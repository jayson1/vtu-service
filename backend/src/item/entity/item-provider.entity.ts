import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { StatusEnum } from '../../common/payloads/enums.payload';
import { ItemCategory } from './item-category.entity';
import { ItemService } from './item-service.entity';
import { TableNames } from '../../common/constants/table-names.constants';

@Entity({ name: TableNames.ITEM_PROVIDER_TBL })
export class ItemProvider {
    /*
    Item provider: the network provider for a service. (ie if service category is Data,
    provider will be MTN, GLO, AIRTEL etc)
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @Column({ type: 'varchar', length: 200, unique: true, name: 'title', nullable: false })
    title: string;

    @Column({ type: 'text', name: 'status' })
    status: StatusEnum;

    @Column({ type: 'boolean', name: 'is_deleted' })
    isDeleted: boolean = false;

    @ManyToOne(() => ItemCategory, (itemCategory) => itemCategory.itemProvider)
    @JoinColumn({ name: 'item_category_id' })
    itemCategory: ItemCategory;

    @OneToMany(() => ItemService, (service) => service.provider)
    itemService: ItemService[];

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;
}
