import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { ItemService } from './item-service.entity';
import { StatusEnum } from '../../common/payloads/enums.payload';
import { TableNames } from '../../common/constants/table-names.constants';

@Entity({ name: TableNames.ITEM_VARIANT_TBL })
export class ItemServiceVariant {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'amount' })
    amount: number;

    @Column({ type: 'boolean', name: 'is_commission_check' })
    isCommissionCheck: boolean = false;

    @Column({ type: 'text', name: 'display', nullable: true })
    display: string;

    @Column({ type: 'boolean', name: 'is_fixed_price' })
    isFixedPrice: boolean = false;

    @Column({ type: 'text', name: 'title', nullable: false })
    title: string;

    @Column({ type: 'integer', name: 'order' })
    order: number = 0;

    @ManyToOne(() => ItemService, (variants) => variants.serviceVariants)
    @JoinColumn({ name: 'item_service_id' })
    itemService: ItemService;

    @Column({ type: 'text', name: 'slug', nullable: true })
    slug: string;

    @Column({ type: 'text', name: 'status' })
    status: StatusEnum = StatusEnum.ACTIVE;
}
