import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { BillerIdentifierEnum, StatusEnum } from '../../common/payloads/enums.payload';
import { ItemServiceVariant } from './item-service-variant.entity';
import { ItemCategory } from './item-category.entity';
import { ItemProvider } from './item-provider.entity';
import { TableNames } from '../../common/constants/table-names.constants';

@Entity({ name: TableNames.ITEM_SERVICE_TBL })
export class ItemService {
    /*
    Item service is a wrapper to group a set of services together called
    service variants. (ie. a category like Data can have providers like MTN, GLO
    and then have services MTNData (this is the service) => (like 150MB MTN Weekly, 300MB MTN etc these granular chunk is
    service variant.
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

    @Column({ type: 'text', name: 'title' })
    title: string;

    @Column({ type: 'text', name: 'slug' })
    slug: string;

    @Column({ type: 'integer', name: 'order' })
    order: number = 0;

    @Column({ type: 'text', name: 'variation_label' })
    variationLabel: string;

    @Column({ type: 'text', name: 'description', nullable: true })
    description: string;

    @Column({ type: 'text', name: 'status' })
    status: StatusEnum;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'min_amount' })
    minAmount: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'max_amount' })
    maxAmount: number;

    @Column({ type: 'boolean', name: 'can_edit_variant_amount' })
    canEditVariantAmount: boolean;

    @Column({ type: 'boolean', name: 'is_validate_biller_code' })
    isValidateBillerCode: boolean;

    @ManyToOne(() => ItemProvider, (provider) => provider.itemService)
    @JoinColumn({ name: 'item_provider_id' })
    provider: ItemProvider; // service provider (ie if service is data, provider will be MTN, GLO, Airtel)

    @ManyToOne(() => ItemCategory, (category) => category.itemService)
    @JoinColumn({ name: 'item_category_id' })
    itemCategory: ItemCategory; // category of this service (ie, DATA, AIRTIME, ELECTRICITY etc)

    @Column({ type: 'numeric', precision: 10, scale: 2, name: 'convenience_fee' })
    convenienceFee: number = 0;

    @Column({ type: 'text', name: 'biller_identifier' })
    billerIdentifier: BillerIdentifierEnum;

    @Column({ type: 'text', name: 'conversion_status', nullable: true })
    conversionStatus: string;

    @Column({ type: 'text', name: 'image_url', nullable: true })
    imageUrl: string;

    @Column({ type: 'text', name: 'share_number', nullable: true })
    shareNumber: string;

    @Column({ type: 'text', name: 'share_info', nullable: true })
    shareInfo: string;

    @Column({ type: 'text', name: 'share_rate', nullable: true })
    shareRate: string;

    @Column({ type: 'text', name: 'more_info', nullable: true })
    moreInfo: string;

    @OneToMany(() => ItemServiceVariant, (variant) => variant.itemService)
    serviceVariants: ItemServiceVariant[];
}
