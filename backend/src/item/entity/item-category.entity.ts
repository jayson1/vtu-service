import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { ServiceCategoryEnum, StatusEnum } from '../../common/payloads/enums.payload';
import { ItemService } from './item-service.entity';
import { ItemProvider } from './item-provider.entity';
import { TableNames } from '../../common/constants/table-names.constants';
import { Plan } from '../../subscription/entity/plan.entity';

@Entity({ name: TableNames.ITEM_CATEGORY_TBL })
export class ItemCategory {
    /*
    item is just a service, this model describes what are the different
    category of service is offered (ie Service category such as DATA, Airtime,
    electricity bills, etc)
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'uuid', type: 'text', nullable: false })
    uuid: string;

    @Column({ type: 'boolean', name: 'is_deleted' })
    isDeleted: boolean = false;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

    @Column({ type: 'varchar', length: 200, unique: true, name: 'title', nullable: false })
    title: string;

    @Column({ type: 'text', name: 'identifier' })
    identifier: ServiceCategoryEnum;

    @Column({ type: 'text', name: 'code' })
    code: string;

    @Column({ type: 'text', name: 'status' })
    status: StatusEnum;

    @OneToMany(() => ItemService, (itemService) => itemService.itemCategory)
    itemService: ItemService[];

    @OneToMany(() => ItemProvider, (itemProvider) => itemProvider.itemCategory)
    itemProvider: ItemProvider[];

    @ManyToOne(() => Plan, { nullable: true })
    @JoinColumn({ name: 'plan_id' })
    plan: Plan;
}
