import { Controller } from '@nestjs/common';
import { AuthService } from '../../auth/service/auth.service';
import { ApiRoutes } from '../../common/constants/api.routes';

@Controller(ApiRoutes.ITEM_ROUTE)
export class ItemController {
    constructor(private authService: AuthService) {}
}
