import { forwardRef, Module } from '@nestjs/common';
import { ItemServiceImpl } from './service/item-service-impl.service';
import { ItemController } from './controller/item.controller';
import { ItemProviderController } from './controller/item-provider.controller';
import { ItemCategoryController } from './controller/item-category.controller';
import { ItemProviderService } from './service/item-provider.service';
import { ItemCategoryService } from './service/item-category.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemCategoryRepository } from './repository/item-category.repository';
import { ItemProviderRepository } from './repository/item-provider.repository';
import { ItemServiceRepository } from './repository/item-service.repository';
import { ItemVariantRepository } from './repository/item-variant.repository';
import { CommonModule } from '../common/common.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ItemCategoryRepository,
            ItemProviderRepository,
            ItemServiceRepository,
            ItemVariantRepository,
        ]),
        forwardRef(() => CommonModule),
    ],
    providers: [ItemServiceImpl, ItemProviderService, ItemCategoryService],
    controllers: [ItemController, ItemProviderController, ItemCategoryController],
    exports: [ItemServiceImpl, ItemProviderService, ItemCategoryService],
})
export class ItemModule {}
