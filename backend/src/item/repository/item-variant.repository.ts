import { EntityRepository, Repository } from 'typeorm';
import { ItemServiceVariant } from '../entity/item-service-variant.entity';

@EntityRepository(ItemServiceVariant)
export class ItemVariantRepository extends Repository<ItemServiceVariant> {}
