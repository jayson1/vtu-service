import { EntityRepository, Repository } from 'typeorm';
import { ItemProvider } from '../entity/item-provider.entity';

@EntityRepository(ItemProvider)
export class ItemProviderRepository extends Repository<ItemProvider> {}
