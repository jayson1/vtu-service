import { EntityRepository, Repository } from 'typeorm';
import { ItemCategory } from '../entity/item-category.entity';

@EntityRepository(ItemCategory)
export class ItemCategoryRepository extends Repository<ItemCategory> {}
