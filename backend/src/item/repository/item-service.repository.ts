import { EntityRepository, Repository } from 'typeorm';
import { ItemService } from '../entity/item-service.entity';

@EntityRepository(ItemService)
export class ItemServiceRepository extends Repository<ItemService> {}
