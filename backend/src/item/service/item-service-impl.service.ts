import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ItemServiceRepository } from '../repository/item-service.repository';
import { ItemService } from '../entity/item-service.entity';
import { ItemVariantRepository } from '../repository/item-variant.repository';
import { ItemServiceVariant } from '../entity/item-service-variant.entity';

@Injectable()
export class ItemServiceImpl {
    private logger = new Logger(ItemServiceImpl.name);
    constructor(
        @InjectRepository(ItemServiceRepository)
        private itemServiceRepo: ItemServiceRepository,

        @InjectRepository(ItemVariantRepository)
        private variantRepository: ItemVariantRepository
    ) {}

    public async createService(itemService: ItemService): Promise<ItemService> {
        try {
            return await this.itemServiceRepo.save(itemService);
        } catch (e) {
            this.logger.error(e);
            throw new InternalServerErrorException(e);
        }
    }

    public async createServiceVariants(variants: ItemServiceVariant[]): Promise<boolean> {
        if (variants.length) {
            await this.variantRepository.save(variants);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    public async getServiceBySlug(slug: string): Promise<ItemService> {
        return await this.itemServiceRepo.findOneOrFail({ slug });
    }

    public async getServiceVariantBySlug(slug: string): Promise<ItemServiceVariant> {
        return await this.variantRepository.findOneOrFail({ slug });
    }

    public async canRunSeeder(): Promise<boolean> {
        const count = await this.itemServiceRepo.count();
        return count < 1;
    }
}
