import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ItemProviderRepository } from '../repository/item-provider.repository';
import { ItemProvider } from '../entity/item-provider.entity';

@Injectable()
export class ItemProviderService {
    constructor(
        @InjectRepository(ItemProviderRepository)
        private itemProviderRepository: ItemProviderRepository
    ) {}

    public async getProviderByTitle(title: string) {
        return this.itemProviderRepository.findOneOrFail({ title });
    }

    public async seedProviders(providers: ItemProvider[]): Promise<boolean> {
        if (providers.length) {
            await this.itemProviderRepository.save(providers);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    public async hasNoData(): Promise<boolean> {
        const count = await this.itemProviderRepository.count();
        return count < 1;
    }
}
