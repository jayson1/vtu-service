import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ItemCategoryRepository } from '../repository/item-category.repository';
import { ItemCategory } from '../entity/item-category.entity';
import { ServiceCategoryEnum } from '../../common/payloads/enums.payload';

@Injectable()
export class ItemCategoryService {
    constructor(
        @InjectRepository(ItemCategoryRepository)
        private itemCategoryRepo: ItemCategoryRepository
    ) {}

    public async getByIdentifier(identifier: ServiceCategoryEnum): Promise<ItemCategory> {
        return this.itemCategoryRepo.findOneOrFail({ identifier });
    }

    public async seedItemCategories(categories: ItemCategory[]): Promise<boolean> {
        if (categories.length) {
            await this.itemCategoryRepo.save(categories);
            return Promise.resolve(true);
        }
        return Promise.resolve(false);
    }

    public async canRunSeeder(): Promise<boolean> {
        const count = await this.itemCategoryRepo.count();
        return count < 1;
    }
}
