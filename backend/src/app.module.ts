import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { MailSmtpModule } from './common/util/mail-smtp/mail-smtp.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { CommonModule } from './common/common.module';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true, expandVariables: true }),
        TypeOrmModule.forRoot({
            type: process.env.DATABASE_TYPE,
            host: process.env.DATABASE_HOST,
            port: parseInt(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USERNAME,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: JSON.parse(process.env.TYPEORM_SYNC),
            autoLoadEntities: JSON.parse(process.env.TYPEORM_LOAD_ENTITIES),
        } as TypeOrmModuleOptions),
        MailSmtpModule,
        ServeStaticModule.forRoot({
            // rootPath: join(__dirname, '..', 'src/_ui/dist'),
            rootPath: join(__dirname, '', 'app'),
        }),
        CommonModule,
    ],
    controllers: [],
    providers: [],
    exports: [CommonModule],
})
export class AppModule {}
