import {
    ConflictException,
    Injectable,
    InternalServerErrorException,
    Logger,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entity/user.entity';
import { Repository } from 'typeorm';
import { SignUpPayload, UserPayload, WalletPayload } from '../../common/payloads/classes.payload';
import { CommonService } from '../../common/service/common.service';
import { ValidationConstant } from '../../common/constants/app.constants';
import { superAdminUsername } from '../../common/payloads/seed-data';
import { WalletService } from '../../wallet/service/wallet.service';

@Injectable()
export class UserService {
    private logger = new Logger(UserService.name);
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private commonService: CommonService,
        private walletService: WalletService
    ) {}

    public async createOne(userPayload: SignUpPayload): Promise<User> {
        try {
            const user: User = new User();
            user.phone = userPayload.phone;
            user.username = userPayload.username;
            user.email = userPayload.email;
            user.isActive = true;
            user.uuid = await this.commonService.generateToken();
            user.salt = await this.commonService.generateSalt();
            user.password = await this.commonService.hashPassword(userPayload.password, user.salt);
            const newUser = await this.userRepository.save(user);
            await this.walletService.createWallet(new WalletPayload(newUser, 0));
            return newUser;
        } catch (err) {
            if (
                err.code === ValidationConstant.DUP_CODE ||
                err.code === ValidationConstant.DUP_TEXT
            ) {
                // duplicate email
                throw new ConflictException(
                    err.sqlMessage ? err.sqlMessage : ValidationConstant.EMAIL_EXIST
                );
            } else {
                throw new InternalServerErrorException(err);
            }
        }
    }

    public async findOneByUuid(uuid: string): Promise<User> {
        const user = await this.userRepository.findOne({ uuid: uuid });
        if (!user) {
            throw new NotFoundException(ValidationConstant.USER_NOT_FOUND);
        }
        return user;
    }

    public async findOneUuidByEmail(email: string): Promise<User> {
        const user = await this.userRepository.findOne({ email: email });
        if (!user) {
            throw new NotFoundException(ValidationConstant.USER_NOT_FOUND);
        }
        return user;
    }

    public async updateOne(userPayload: UserPayload): Promise<boolean> {
        return true;
    }

    public async updateUserPassword(user: User, password: string): Promise<boolean> {
        try {
            user.salt = await this.commonService.generateSalt();
            user.password = await this.commonService.hashPassword(password, user.salt);
            await this.userRepository.save(user);
            return Promise.resolve(true);
        } catch (e) {
            this.logger.error(e.message, e);
            return Promise.resolve(false);
        }
    }

    public findAll(): Promise<User[]> {
        return this.userRepository.find();
    }

    public findOneById(uuid: string): Promise<User> {
        return this.userRepository.findOneOrFail({ uuid: uuid });
    }

    public async findOneByEmail(email: string): Promise<User> {
        const user = await this.userRepository.findOne({ email });
        if (!user) {
            throw new NotFoundException(ValidationConstant.USER_NOT_FOUND);
        }
        return user;
    }

    public async removeOne(uuid: string): Promise<void> {
        await this.userRepository.delete(uuid);
    }

    public async disableUser(uuid: string): Promise<void> {}

    public async updateUserIsVerified(uuid: string, status: boolean): Promise<boolean> {
        const user = await this.findOneByUuid(uuid);
        if (user) {
            user.isVerified = status;
            const saved = await this.userRepository.save(user);
            return saved.isVerified;
        }
        return Promise.resolve(false);
    }

    public async validateUserPassword(user: User, password: string): Promise<boolean> {
        return await this.isPasswordMatch(user.salt, user.password, password);
    }

    public async seedSystemUser(seed: User): Promise<boolean> {
        try {
            const sUser = await this.userRepository.save(seed);
            await this.walletService.createWallet(new WalletPayload(sUser, 10000));
            return Promise.resolve(true);
        } catch (e) {
            this.logger.error(e);
            return Promise.resolve(false);
        }
    }

    public async canSeed(): Promise<boolean> {
        const user = await this.userRepository.findOne({ username: superAdminUsername });
        return !user;
    }

    private async isPasswordMatch(
        salt: string,
        storedPassword: string,
        claimPassword: string
    ): Promise<boolean> {
        const hash = await this.commonService.hashPassword(claimPassword, salt);
        return hash === storedPassword;
    }
}
