import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    OneToOne,
    OneToMany,
} from 'typeorm';
import { UserTypeEnum } from '../../common/payloads/enums.payload';
import { Exclude } from 'class-transformer';
import { TableNames } from '../../common/constants/table-names.constants';
import { Wallet } from '../../wallet/entity/wallet.entity';
import { Transaction } from '../../transaction/entity/transaction.entity';

@Entity({ name: TableNames.USERS_TBL })
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text', name: 'uuid' })
    uuid: string;

    @Exclude()
    @CreateDateColumn({ name: 'created_at' })
    createdAt: Date;

    @Exclude()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt: Date;

    @Column({ type: 'text', name: 'first_name', nullable: true })
    firstName: string;

    @Column({ type: 'text', name: 'last_name', nullable: true })
    lastName: string;

    @Column({ type: 'varchar', name: 'email', unique: true, length: 200 })
    email: string;

    @Column({ type: 'text', name: 'username', nullable: false })
    username: string;

    @Exclude()
    @Column({ name: 'salt', nullable: false, type: 'text' })
    salt: string;

    @Exclude()
    @Column({ name: 'password', nullable: false, type: 'text' })
    password: string;

    @Column({ type: 'text', name: 'phone' })
    phone: string;

    @Column({ default: false, type: 'boolean', name: 'is_active' })
    isActive: boolean;

    @Column({ default: false, type: 'boolean', name: 'is_verified' })
    isVerified: boolean;

    @Column({ type: 'text', name: 'type', nullable: false })
    type: UserTypeEnum = UserTypeEnum.REGULAR;

    @OneToOne(() => Wallet, (wallet) => wallet.user, { nullable: true })
    wallet: Wallet;

    @OneToMany(() => Transaction, (transaction) => transaction.user)
    transactions: Transaction[];
}
